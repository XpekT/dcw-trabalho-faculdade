# Título

Projecto para a disciplina de Desenvolvimento de Conteúdos Web - RSI

# Tecnologias

HTML 5  
CSS3  
PHP   
Javascript

** NOTA **  
Não foram usadas quaisquer frameworks neste projecto. 
O website foi desenvolvido com a filosofia "mobile-first" pelo que é totalmente responsivo e adptativo a qualquer dispositivo >= 320px de largura.

# Ambiente de Produção

http://dcwteste.000webhostapp.com/trabalho_um/

# Autoria

Bruno Moreira
