<?php

require "vendor/autoload.php";

use DCW\Classes\Session;

Session::start();

if(Session::isActive('session-user')) {
	$userSession = Session::get('session-user');
	header("Location: /profile.php?hid=" . $userSession['hid']);
	exit();
}

$countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");

?>

<?php include_once(__DIR__ . '/includes/imports.php') ?>
		
		<?php include_once(__DIR__ . '/includes/nav.php') ?>

        <div class="main-container">

			<div class="signin-wrapper">
				<h1><i class="fas fa-user-circle"></i> Já possui conta? Entrar!</h1>
				<form id="login-form" method="POST">
					<div class="input-wrapper">
						<label for="email-login"><i class="fas fa-at"></i> Email</label>
						<input type="email" name="email" id="email-login">
					</div>
					<div class="input-wrapper">
						<label for="password_signin"><i class="fas fa-lock-open"></i> Palavra-Passe</label>
						<input type="password" name="password" id="password_signin">
					</div>
					<div class="button-wrapper">
						<button type="submit">Entrar!</button>
						<button type="reset" class="reset-button">Limpar!</button>
						<button type="button" class="forgot-button" id="forgot-button"><i class="fas fa-info-circle"></i> Não sei a minha password!</button>
						<button type="button" class="resend-button" id="resend-button"><i class="fas fa-redo"></i> Reenviar o email de ativação!</button>
					</div>
					<div class="error-wrapper" id="error-login-form"></div>
					<div class="spinner" id="login-form-spinner">
						<div class="bounce1"></div>
						<div class="bounce2"></div>
						<div class="bounce3"></div>
					</div>
				</form>
			</div>

			<div class="separator"></div>
			
			<div class="signup-wrapper">
				<h1><i class="fas fa-key"></i> Não possui conta? Registar!</h1>
				<form id="register-form" method="POST">
					<div class="input-wrapper">
						<label for="name"><i class="fas fa-user"></i> Nome Completo</label>
						<input type="text" name="name" id="name">
					</div>
					<div class="input-wrapper">
						<label for="email"><i class="fas fa-at"></i> Email</label>
						<input type="email" name="email" id="email">
					</div>
					<div class="input-wrapper">
						<label for="date-of-birth"><i class="fas fa-calendar"></i> Data de Nascimento</label>
						<div id="date-of-birth"></div>
					</div>
					<div class="input-wrapper">
						<label for="nationality"><i class="fas fa-globe-africa"></i> Nacionalidade</label>
						<select name="nationality" id="nationality">
							<?php  foreach($countries as $country): ?>
								<?php echo '<option value='.$country.'>'.$country.'</option>'; ?>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="input-wrapper">
						<label for="country"><i class="fas fa-flag"></i> País</label>
						<select name="country" id="country">
							<?php  foreach($countries as $country): ?>
								<?php echo '<option value='.$country.'>'.$country.'</option>'; ?>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="input-wrapper">
						<label for="city"><i class="fas fa-map-signs"></i> Cidade</label>
						<input type="text" name="city" id="city">
					</div>
					<div class="input-wrapper" id="cartao_cidadao-wrapper">
						<label for="cartao_cidadao"><i class="fas fa-id-card"></i> Número do Cartão de Cidadão</label>
						<input type="text" name="cartao_cidadao" id="cartao_cidadao">
					</div>
					<div class="input-wrapper" id="passport-wrapper">
						<label for="passport"><i class="fas fa-id-card"></i> Número do Passaporte</label>
						<input type="text" name="passport" id="passport">
					</div>
					<div class="input-wrapper">
						<label for="password"><i class="fas fa-lock"></i> Palavra-Passe</label>
						<input type="password" name="password" id="password">
					</div>
					<div class="input-wrapper">
						<label for="password-confirmation"><i class="fas fa-lock"></i> Confirme a Palavra-Passe</label>
						<input type="password" name="password-confirmation" id="password-confirmation">
					</div>
					<div class="button-wrapper">
						<button type="submit" id="button-register">Registar!</button>
						<button type="reset" id="button-reset-register">Limpar!</button>
					</div>
					<div class="error-wrapper" id="error-register-form"></div>
					<div class="spinner" id="register-form-spinner">
						<div class="bounce1"></div>
						<div class="bounce2"></div>
						<div class="bounce3"></div>
					</div>
				</form>
			</div>

			<div id="forgot-password-dialog" title="Recuperação de conta">
				<input type="email" name="email" id="email-forgot" placeholder="Email" class="text ui-widget-content ui-corner-all">
				<input type="hidden" name="safety" id="safety" value=''>
				<div class="forgot-password-message-wrapper"></div>
			</div>

		</div>

		<?php include_once(__DIR__ . '/includes/footer.php'); ?>

		<script type="text/javascript">

			$( function() {

				let controller = 'forgot'

				// Cartão de Cidadão e Passaporte - Toggling
				$('#cartao_cidadao').on('keyup', function() {
					
					if($(this).val().length === 0) {
						$('#passport-wrapper').show()
					} else {
						$('#passport-wrapper').hide()
					} 
				
				})

				$('#passport').on('keyup', function() {
					
					if($(this).val().length === 0) {
						$('#cartao_cidadao-wrapper').show()
					} else {
						$('#cartao_cidadao-wrapper').hide()
					}

				})

				// Formulário de Registo
				$('#register-form').submit(function(event) {

					event.preventDefault()

					// Limpar os erros do formulário a cada submissão
					$('#error-register-form').html('')

					// Mostrar o loading spinner
					$('#register-form-spinner').show()

					// Formulário - jQuery Wrapper
					let formData = $(this)[0]

					// Campos do formulário
					let data = {
						name: $(formData[0]).val(),
						email: $(formData[1]).val(),
						dob: $("#date-of-birth").val(),
						nationality: $(formData[4]).val(),
						country: $(formData[5]).val(),
						city: $(formData[6]).val(),
						cc: $(formData[7]).val().length !== 0 ? $(formData[7]).val() : null,
						passport: $(formData[8]).val().length !== 0 ? $(formData[8]).val() : null,
						password: $(formData[9]).val(),
						password_confirmation: $(formData[10]).val()
					}

					// Pedido AJAX
					$.ajax({
						url: '/app/DCW/Controllers/register.php',
						type: 'POST',
						data: data,
						dataType: 'json',
						beforeSend: function() {
							
							// Avalia se as palavra-passe são iguais sem a necessidade de contactar o servidor
							if(data.password !== data.password_confirmation) {
								
								$('#register-form-spinner').hide()

								$('#error-register-form').html(
									'<p class="form-error"><i class="fas fa-exclamation-triangle"></i> As palavras-passe não são iguais!</p>'
								)

								return false
							}

							// Verifica se nenhum número de identificação foi adicionado
							if(data.cc === null && data.passport === null) {

								$('#register-form-spinner').hide()

								$('#error-register-form').html(
									'<p class="form-error"><i class="fas fa-exclamation-triangle"></i> Tem de fornecer um número de identificação!</p>'
								)

								return false

							}
						
						}
					})
					.done(function(data) {
						
						$('#register-form-spinner').hide()

						if(data.status !== 200) {
							$('#error-register-form').html(
								'<p class="form-error"><i class="fas fa-exclamation-triangle"></i> ' + data.error + '</p>'
							)
						} else {

							// Desabilita o botão de envio para evitar o button spamming - não é um método seguro!
							$('#button-register').attr('disabled', 'disabled')

							// Limpa o formulário
							$('#button-reset-register').click()

							$('#error-register-form').html(
								'<p class="form-success"><i class="fas fa-check-circle"></i> ' + data.message + '</p>'
							)
						}

					})

				})

				// Formulário de Login
				$('#login-form').submit(function(event) {

					event.preventDefault()

					// Limpar os erros do formulário a cada submissão
					$('#error-login-form').html('')

					// Mostrar o loading spinner
					$('#login-form-spinner').show()

					// Formulário - jQuery Wrapper
					let formData = $(this)[0]

					// Campos do formulário
					let data = {
						email: $(formData[0]).val(),
						password: $(formData[1]).val()
					}

					// Pedido AJAX
					$.ajax({
						url: '/app/DCW/Controllers/login.php',
						type: 'POST',
						data: data,
						dataType: 'json',
						beforeSend: function() {
							
							// Avalia se as palavra-passe são iguais sem a necessidade de contactar o servidor
							if(data.email.length === 0 || data.password.length === 0) {
								
								$('#login-form-spinner').hide()

								$('#error-login-form').html(
									'<p class="form-error"><i class="fas fa-exclamation-triangle"></i> Tem de preencher todos os campos!</p>'
								)

								return false
							}
						
						}
					})
					.done(function(data) {
						
						$('#login-form-spinner').hide()

						if(data.status !== 200) {
							
							$('#error-login-form').html(
								'<p class="form-error"><i class="fas fa-exclamation-triangle"></i> ' + data.error + '</p>'
							)
						
							$('#login-form .reset-button').click()

						} else {

							// Redirecionar o utilizador para o seu perfil
							let hid = data.hid

							window.location.href = '/profile.php?hid=' + hid

						}

					})

				})

				function sendEmail() {

					let email = $('#email-forgot').val()
					let safety = $('#safety').val()
					let emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/
					let messageWrapper = $('.forgot-password-message-wrapper')

					messageWrapper.html('')

					$.ajax({
						url: '/app/DCW/Controllers/' + controller + '.php',
						type: 'POST',
						data: { email },
						dataType: 'json',
						beforeSend: function() {
							
							let isValid = false

							if(safety.length !== 0) {
								return false
							}

							if(!(emailRegex.test(email)) || email.length === 0) {

								messageWrapper.html('<p class="error"><i class="fas fa-info-circle"></i> Tem de fornecer um email válido!</p>')
							
							} else {
								
								isValid = true
								$('#blocker').fadeIn('fast')
							
							}

							return isValid

						}
					})
					.done(function(data) {

						$('#blocker').fadeOut('slow', function() {

							$('#email-forgot').val('')

							if(data.status === 200) {
								messageWrapper.html('<p class="success"><i class="fas fa-check-circle"></i> ' + data.message + '</p>')
							} else {
								messageWrapper.html('<p class="error"><i class="fas fa-info-circle"></i> ' + data.error + '</p>')
							}
						
						})

					})
					
				}

				$('#forgot-password-dialog').dialog({
					autoOpen: false,
					draggable: false,
					height: 'auto',
					width: 450,
					modal: true,
					buttons: {
						"Confirmar": sendEmail,
						Cancel: function() {
							dialog.dialog( "close" );
						}
					},
					close: function() {
						$('#email-forgot').val('')
						$('.forgot-password-message-wrapper').html('')
					}
				})

				// Reset da password do utilizador
				$('#forgot-button').on('click', function() {
					controller = 'forgot'
					$('#forgot-password-dialog').dialog('open')
				})

				// Reenviar email de ativação ao utilizador
				$('#resend-button').on('click', function() {
					controller = 'resend'
					$('#forgot-password-dialog').dialog('open')
				})

				// Calendário
				$('#date-of-birth').datepicker({
					changeMonth: true,
					changeYear: true,
					monthNamesShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"]
				})

			})

		</script>

    </body>

</html>
    