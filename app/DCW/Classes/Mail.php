<?php

namespace DCW\Classes;

use Swift_SmtpTransport;
use Swift_Mailer;
use Swift_Message;

class Mail {

    protected $transport;
    protected $mailer;
    protected $message;

    public function __construct()
    {
        $this->transport = new Swift_SmtpTransport();
        $this->transport->setUsername();
        $this->transport->setPassword();                                       

        $this->mailer = new Swift_Mailer($this->transport);

        $this->message = new Swift_Message();
    }

    public function createMessage($subject, $from, $to, $body)
    {
        return $this->message->setSubject($subject)
                             ->setFrom($from)
                             ->setTo($to)
                             ->setBody($body, 'text/html')
                             ->addPart($body);
    }

    public function sendMessage()
    {
        return $this->mailer->send($this->message);
    }


}