<?php

namespace DCW\Classes;

class Session {
    
    public static function start()
	{
		if(session_status() === PHP_SESSION_NONE) {
			session_start();
		}
		
		session_regenerate_id(true);
	}

	public static function isActive($name)
	{
		if(isset($_SESSION[$name])) {
			return true;
		}

		return false;
	}

	public static function check($name, $field = null, $value)
	{
		if(self::isActive($name)) {

			if($field !== null) {

				if(self::get($name)->$field === $value) {
					return true;
				}
			}

			if(self::get($name) === $value) {
				return true;
			}

			return false;
		}

		return false;

	}

	public static function clear($name)
	{
		if(self::isActive($name)) {
			unset($_SESSION[$name]);
		}

		return NULL;
	}

	public static function get($name)
	{
		if(self::isActive($name)) {
			return $_SESSION[$name];
		}

		return NULL;
	}

	public static function hasSession($type)
	{
		return isset($_SESSION[$type]) ? true : false;
	}

	public static function set($name, $value = [])
	{
		return $_SESSION[$name] = $value;
	}

	public static function setField($name, $field, $value) {
		return $_SESSION[$name][$field] = $value;
	}

}