<?php

require '../../../vendor/autoload.php';

use DCW\Models\User;

if(isset($_GET['email'])) {

    $email = $_GET['email'];

    $user = new User();

    $result = $user->activate($email);
    
    if($result['status'] === True) {

        $hid = $result['hid'];

        // Redirecionar para página de utilizador
        header("Location: /profile.php?hid=$hid");
        exit();

    } 

    header("Location: /");
    exit();
    
}