<?php

require "../../../vendor/autoload.php";

use DCW\Models\Reservation;

if(isset($_POST['pickedDate']) && mb_strlen($_POST['pickedDate']) !== 0 && isset($_POST['iid']) && mb_strlen($_POST['iid']) !== 0) {

    // Buscar a reservar
    $reservation = new Reservation();

    echo json_encode([
        "status" => 200,
        "dates" => $reservation->getAvailableDates($_POST['pickedDate'], $_POST['iid'])
    ]);

    return;

} else {

    echo json_encode([
        "status" => 403,
        "error" => "De momento não é possível processar o seu pedido. Por favor, tente mais tarde."
    ]);

    return;

}