<?php

require "../../../vendor/autoload.php";

use DCW\Models\Reservation;

if(isset($_POST['rid']) && mb_strlen($_POST['rid']) !== 0) {

    // Buscar a reservar
    $reservation = new Reservation();
    
    $result = $reservation->cancelReservation($_POST['rid']);

    echo json_encode($result);
    
    return;


}