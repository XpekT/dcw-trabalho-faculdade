<?php

namespace DCW\Controllers;

require '../../../vendor/autoload.php';

use DCW\Helpers\Utils;

if(isset($_POST['generate'])) {

    $imageName = Utils::generateCaptcha();

    echo json_encode([
        'status' => 200,
        'message' => 'Generated!',
        'imageName' => $imageName
    ]);

    return;

}