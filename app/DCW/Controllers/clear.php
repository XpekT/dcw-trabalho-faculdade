<?php

require "../../../vendor/autoload.php";

use DCW\Classes\Session;

Session::start();

$sessionType = isset($_POST['type']) ? $_POST['type'] : NULL;

Session::clear($sessionType);

echo json_encode([
    'cleared' => true
]);

return;