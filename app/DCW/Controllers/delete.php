<?php

require "../../../vendor/autoload.php";

use DCW\Models\User;

if(isset($_POST['hid']) && mb_strlen($_POST['hid']) !== 0) {

    // Processar a eliminação de conta
    $user = new User();
    
    $result = $user->delete($_POST['hid']);

    echo json_encode($result);
    
    return;


}