<?php

require "../../../vendor/autoload.php";

use DCW\Models\User;

if(isset($_POST['hid']) && mb_strlen($_POST['hid']) !== 0 && isset($_POST['data'])) {

    // Processar à edição da conta
    $user = new User();
    
    $result = $user->edit($_POST['hid'], $_POST['data']);

    echo json_encode($result);
    
    return;


}