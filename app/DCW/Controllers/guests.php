<?php 

require '../../../vendor/autoload.php';

use DCW\Models\Reservation;

if(isset($_POST['rid']) && mb_strlen($_POST['rid']) !== 0 && isset($_POST['data']) && isset($_POST['totalGuests'])) {

    $reservation = new Reservation();

    $result = $reservation->addGuests($_POST);

    if($result['status'] === 200) {

        echo json_encode([
            'status' => 200,
            'data' => $result['data']
        ]);
    
        return;
    
    }


}

echo json_encode([
    'status' => 403,
    'cancel' => True
]);

return;