<?php

namespace DCW\Controllers;

require '../../../vendor/autoload.php';

use DCW\Classes\Mail;
use DCW\Classes\Session;
use DCW\Helpers\Sanitize;
use DCW\Helpers\Utils;

if(session_status() === PHP_SESSION_NONE) {
    Session::start();
}

if(isset($_POST['name']) && isset($_POST['email']) && isset($_POST['contact']) && isset($_POST['subject']) && isset($_POST['message']) && isset($_POST['captcha'])) {

    $name = Sanitize::clean($_POST['name']);
    $email = Sanitize::clean($_POST['email']);
    $contact = Sanitize::clean($_POST['contact']);
    $subject = Sanitize::clean($_POST['subject']);
    $message = Sanitize::clean($_POST['message']);
    $captchaFromForm = Sanitize::clean($_POST['captcha']);

    $captchaFromSession = Session::get('captcha-phrase');

    // Se os captchas forem iguais - processar email
    if($captchaFromForm === $captchaFromSession) {

        $imageName = Utils::generateCaptcha();

        $mail = new Mail();

        $mail->createMessage(
            'Pedido de Informações - ' . $subject,
            'admin@yeswerent.dx.am',
            'admin@yeswerent.dx.am',
            '<h1>Pedido de Informações!</h1><p>' . $message . '</p><h3>Dados do Utilizador:</h3><hr><p><strong>Nome:</strong> '. $name .'</p><p><strong>Email: </strong> '. $email .'</p><p><strong>Contato: </strong> ' . $contact . '</p>'
        );

        try {

            if($mail->sendMessage()) {

                echo json_encode([
                    'status' => 200,
                    'message' => "Mensagem Enviada! Tentaremos ser o mais breves possíveis na resposta.",
                    'imageName' => $imageName
                ]);

                return;

            }

        } catch (\Exception $e) {

            $imageName = Utils::generateCaptcha();

            echo json_encode([
                'status' => 403,
                'error' => 'De momento não é possível processar o seu pedido. Por favor, tente mais tarde.',
                'imageName' => $imageName
            ]);

            return;

        }

    } else {

        $imageName = Utils::generateCaptcha();

        echo json_encode([
            'status' => 403,
            'error' => 'Captcha Errado. Tente novamente.',
            'imageName' => $imageName
        ]);

        return;

    }

}

$imageName = Utils::generateCaptcha();

echo json_encode([
    'status' => 403,
    'error' => 'De momento não é possível processar o seu pedido. Por favor, tente mais tarde.',
    'imageName' => $imageName
]);

return;