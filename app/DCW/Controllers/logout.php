<?php

namespace DCW\Controllers;

require '../../../vendor/autoload.php';

use DCW\Models\User;

if(isset($_POST['logout']) && $_POST['logout']) {

    $user = new User();

    if($user->logout()) {
        
        echo json_encode([
            "status" => 200
        ]);
    
        return;
    }

} else {

    header('Location: /');

}