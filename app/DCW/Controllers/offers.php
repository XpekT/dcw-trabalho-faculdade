<?php

require "../../../vendor/autoload.php";

use DCW\Models\Property;

if(isset($_POST['offset'])) {

    $property = new Property();

    echo json_encode($property->chunk($_POST['offset']));

    return;

}

echo json_encode([
    'status' => 403
]);

return;