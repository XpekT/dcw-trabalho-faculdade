<?php

require "../../../vendor/autoload.php";

use DCW\Models\Property;

if(isset($_POST['iid']) && mb_strlen($_POST['iid']) !== 0) {

    $property = new Property();

    echo json_encode([
        "status" => 200,
        "property" => $property->get($_POST['iid'])
    ]);

    return;

} else {

    echo json_encode([
        "status" => 403,
        "error" => "De momento não é possível processar o seu pedido. Por favor, tente mais tarde."
    ]);

    return;

}