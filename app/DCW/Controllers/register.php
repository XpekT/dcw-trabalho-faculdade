<?php 

namespace DCW\Controllers;

require '../../../vendor/autoload.php';

use DCW\Models\User;
use DCW\Helpers\Forms;

if(!Forms::isDirty($_POST)) {

    $user = new User();
    
    $result = $user->register($_POST);

    if($result['status'] === 200) {
        
        echo json_encode($result);

        return;
    
    } else {

        echo json_encode($result);

        return;

    }

} else {

	echo json_encode([
        "status" => 403,
        "error" => "Tem de preencher todos os campos do formulário!"
    ]);

    return;

}
