<?php

require "../../../vendor/autoload.php";

use DCW\Models\User;

if(isset($_POST['email']) && mb_strlen($_POST['email']) !== 0) {

    $user = new User();

    echo json_encode($user->resendActivation($_POST['email']));

    return;

}

echo json_encode([
    'status' => 403,
    'error' => 'De momento, não podemos processar o seu pedido.'
]);

return;