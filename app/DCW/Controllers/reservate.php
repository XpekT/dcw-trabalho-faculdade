<?php

require "../../../vendor/autoload.php";

use DCW\Models\Reservation;

if(isset($_POST['entryDate']) && mb_strlen($_POST['entryDate']) !== 0 && isset($_POST['leaveDate']) && mb_strlen($_POST['leaveDate']) !== 0 && isset($_POST['iid']) && isset($_POST['numberOfGuests']) && isset($_POST['price'])) {

    // Buscar a reservar
    $reservation = new Reservation();
    
    $result = $reservation->makeReservation($_POST);

    echo json_encode($result);
    
    return;


}
