<?php

require "../../../vendor/autoload.php";

use DCW\Models\Property;

if(isset($_POST['search']) && mb_strlen($_POST['search']) > 3) {

    $property = new Property();

    $results = $property->search($_POST['search']);

    echo json_encode([
        'status' => 200,
        'results' => $results
    ]);

    return;

}