<?php

namespace DCW;

class Database {

    private $connection = null;

    private $config = [
        'development' => [
            'host' => 'localhost',
            'user' => 'xpekt',
            'password' => '12345',
            'database' => 'dcw'
        ],
        'production' => [
            'host' => '',
            'user' => '',
            'password' => '',
            'database' => ''
        ]
    ];

    protected $env = [
        'development' => 'http://localhost:8080',
        'production' => ''
    ];

    function __construct() {
        
        $this->connection = mysqli_connect(
            $this->config['development']['host'],
            $this->config['development']['user'],
            $this->config['development']['password'],
            $this->config['development']['database']
        );
        
        $this->connection->set_charset("utf8");
    
    }


    public function getConnection() {
        return $this->connection;
    }

    function __destruct() {
        mysqli_close($this->connection);
    }

}