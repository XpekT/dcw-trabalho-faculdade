<?php

namespace DCW\Helpers;

class Forms {

    public static function isDirty($values = []) {

        $dirty = true;

        if(isset($values) && count($values) !== 0) {

            foreach($values as $value) {
                if(empty($value) || mb_strlen($value) === 0) {
                    $dirty = true;
                } else {
                    $dirty = false;
                }
            }
        
        }

        return $dirty;

    }

}