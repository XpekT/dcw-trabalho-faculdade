<?php

namespace DCW\Helpers;

use DCW\Classes\Session;
use Gregwar\Captcha\CaptchaBuilder;
use ShortCode\Random as ShortCode;
use DateTime;

if(session_status() === PHP_SESSION_NONE) {
    Session::start();
}

class Utils {

    public static function isLaterThan($date = NULL) {

        $now = new DateTime('now');
        $candidate = new DateTime($date);

        return $candidate > $now;

    }

    public static function dateFormat($date = NULL) {
        $date = new DateTime($date);
        return $date->format('d/m/Y');
    }

    public static function generateCaptcha() {


        $dir = '../../../images';

        if(is_dir($dir)) {

            if($dh = opendir($dir)) {

                while(($file = readdir($dh)) !==  False) {

                    $captchaFilename = explode('-', $file);

                    if($captchaFilename[0] === 'captcha') {
                        unlink($dir . '/' . $file);
                    }

                }

                closedir($dh);

            }

        }

        $imageName = "captcha-" . ShortCode::get();

        // Criar o captcha
        $builder = new CaptchaBuilder();
        $builder->build();
        $builder->save('../../../images/'. $imageName .'.jpg');

        // Salvar a resposta na session
        Session::set('captcha-phrase', $builder->getPhrase());
        Session::set('captcha-image-name', $imageName);

        return $imageName;

    }

}