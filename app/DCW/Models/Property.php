<?php

namespace DCW\Models;

use DCW\Database;
use DCW\Helpers\Sanitize;

class Property extends Database {

    public function get($iid = NULL) {
        
        // Limpar parâmetro
        $iid = Sanitize::clean($iid);

        // Buscar imóvel
        $query = 
        "SELECT * FROM Properties
        WHERE iid = '$iid'";

        $result = $this->getConnection()->query($query);

        $rows = [];

        if($result) {

            while($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }
    
            $result->free();
        
        }

        return $rows;

    }

    public function getReservedDates($iid = NULL) {

        $iid = Sanitize::clean($iid);

        $query = 
        "SELECT DATE_FORMAT(Reservations.data_entrada, '%Y-%m-%d') AS data_entrada, DATE_FORMAT(Reservations.data_saída, '%Y-%m-%d') AS data_saída FROM Reservations
        INNER JOIN Properties ON Reservations.iid = Properties.iid AND Properties.iid = '$iid'
        WHERE data_entrada >= DATE_FORMAT(NOW(), '%Y-%m-%d')";

        $result = $this->getConnection()->query($query);

        $rows = [];

        if($result) {

            while($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }

            $result->free();

        }

        return $rows;

    }

    public function getCapacity($iid = NULL) {

        $iid = Sanitize::clean($iid);

        $query = "SELECT lotação FROM Properties WHERE iid = '$iid'";

        $result = $this->getConnection()->query($query);

        $rows = [];

        if($result) {

            while($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }

        }

        return $rows;

    }

    public function chunk($offset = 0) {

        $offset = Sanitize::clean($offset);

        $query =
        "SELECT * FROM Properties LIMIT 9 OFFSET $offset";

        $result = $this->getConnection()->query($query);

        $rows = [];

        if($result) {

            while($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }

            $result->free();

        }

        return $rows;

    }

    public function search($search = NULL) {

        $search = Sanitize::clean($search);

        $query = 
        "SELECT * FROM Properties WHERE título LIKE '%$search%'
        OR cidade LIKE '%$search%' OR localização LIKE '%$search%'
        OR preço_diário LIKE '%$search%' 
        ORDER BY adicionado_a DESC 
        LIMIT 20";

        $result = $this->getConnection()->query($query);

        $rows = [];

        if($result) {

            while($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }

            $result->free();

        }

        return $rows;

    }

}