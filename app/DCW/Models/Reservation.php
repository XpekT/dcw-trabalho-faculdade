<?php

namespace DCW\Models;

use DCW\Database;
use DCW\Helpers\Sanitize;
use DCW\Classes\Session;
use DCW\Models\User;
use DCW\Models\Property;
use DateTime;
use ShortCode\Random as ShortCode;

if(session_status() === PHP_SESSION_NONE) {
    Session::start();
}

class Reservation extends Database {

    public function belongsToReservation($rid = NULL, $hid = NULL) {

        // Limpar os parâmetros
        $rid = Sanitize::clean($rid);
        $hid = Sanitize::clean($hid);

        if($rid === NULL || empty($rid) && $hid === NULL || empty($hid)) {

            return false;

        } else {

            // Query
            $query = 
            "SELECT * FROM Guests_Reservations WHERE hid = '$hid' AND rid = '$rid'";

            $result = $this->getConnection()->query($query);

            $rows = [];

            if($result) {

                while($row = $result->fetch_assoc()) {
                    array_push($rows, $row);
                }

                $result->free();
            }

            return count($rows) > 0;
        
        }

    }

    public function getReservation($rid = NULL) {

        // Limpar o parâmetro
        $rid = Sanitize::clean($rid);

        $query = 
        "SELECT Guests.hid, Guests.nome, Reservations.*, Properties.iid, Properties.título, Properties.foto_principal, Properties.localização FROM Guests_Reservations
        INNER JOIN Guests ON Guests_Reservations.hid = Guests.hid
        INNER JOIN Reservations ON Guests_Reservations.rid = Reservations.rid
        INNER JOIN Properties ON Properties.iid = Reservations.iid
        WHERE Guests_Reservations.rid = '$rid'";

        $result = $this->getConnection()->query($query);

        $rows = [];
        
        if($result) {
            
            while($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }

            $result->free();

        }

        return $rows;

    }

    public function getAvailableDates($pickedDate = NULL, $iid = NULL) {

        // Limpar o parâmetro
        $pickedDate = Sanitize::clean($_POST['pickedDate']);
        $iid = Sanitize::clean($_POST['iid']);

        // Buscar as datas em que o imóvel se encontra reservado
        $query = 
        "SELECT Properties.preço_diário, DATE_FORMAT(Reservations.data_entrada, '%Y-%m-%d') AS data_entrada, DATE_FORMAT(Reservations.data_saída, '%Y-%m-%d') AS data_saída
        FROM Reservations
        INNER JOIN Properties ON Reservations.iid = Properties.iid AND Reservations.iid = '$iid'
        WHERE '$pickedDate' BETWEEN data_entrada AND data_saída";

        $result = $this->getConnection()->query($query);

        $rows = [];

        while ($row = $result->fetch_assoc()) {
            array_push($rows, $row);
        }

        // Liberta o conjunto
        $result->free();

        return $rows;

    }

    public function getLastReservation() {

        $query = "SELECT * FROM Reservations ORDER BY rid DESC LIMIT 1";

        $result = $this->getConnection()->query($query);

        $rows = [];

        if($result) {

            // Agrega as linhas num array
            while ($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }

            // Liberta o conjunto
            $result->free();
        
        }

        return $rows[0];

    }

    public function makeReservation($data = NULL) {

        // Limpar os parâmetros
        $entryDate = Sanitize::clean($data['entryDate']);
        $leaveDate = Sanitize::clean($data['leaveDate']);
        $numberOfAdditionalGuests = (int)Sanitize::clean($data['numberOfGuests']);
        $iid = (int)Sanitize::clean($data['iid']);
        $price = (float)Sanitize::clean($data['price']);
        $titular = (int)Session::get('session-user')['hid'];
        $código = "RES." . ShortCode::get();

        $entryDate = date_create($entryDate);
        $entryFormattedDate = date_format($entryDate, 'Y-m-d');

        $leaveDate = date_create($leaveDate);
        $leaveFormattedDate = date_format($leaveDate, 'Y-m-d');

        // Verificar se o número de hóspedes adicionais ultrapassa a lotação do imóvel
        $fullGuests = $numberOfAdditionalGuests + 1;

        $property = new Property();

        $capacity = $property->getCapacity($iid)[0]['lotação'];

        if($fullGuests > $capacity) {
            
            return [
                'status' => 403,
                'error' => 'A lotação máxima do imóvel são ' . $capacity . ' pessoas.'
            ];
        
        } else {

            // Introduzir a reserva
            $query = 
            "INSERT INTO Reservations (data_entrada, data_saída, valor_total, hóspedes_adicionais, código, iid, titular)
            VALUES ('$entryFormattedDate', '$leaveFormattedDate', '$price', '$numberOfAdditionalGuests', '$código', '$iid', '$titular')";
    
            if($this->getConnection()->query($query)) {

                // Buscar a rid da reserva recém criada
                $query = "SELECT rid FROM Reservations ORDER BY rid DESC LIMIT 1";

                $result = $this->getConnection()->query($query);

                $rows = [];

                if($result) {

                    while($row = $result->fetch_assoc()) {
                        array_push($rows, $row);
                    }

                    $result->free();

                }

                // Atualizar a tabela intermédia Guests_Reservations
                $rid = $rows[0]['rid'];
                
                $query = "INSERT INTO Guests_Reservations(hid, rid) VALUES('$titular', '$rid')";

                if($this->getConnection()->query($query)) {

                    if($numberOfAdditionalGuests === 0) {
                        Session::set('reservation-success', 'A sua reserva foi registada com sucesso!');
                    }

                    return [
                        'status' => 200,
                        'message' => 'A sua reserva foi registada com sucesso!',
                        'rid' => $rows[0]['rid']
                    ];
                
                } else {

                    return [
                        'status' => 403,
                        'error' => 'De momento não é possível processar a sua reserva.<br>Por favor, tente mais tarde.'
                    ];
                
                }


            } else {

                return [
                    'status' => 403,
                    'error' => 'De momento não é possível processar a sua reserva.<br>Por favor, tente mais tarde.'
                ];
            
            }
        
        }


    }

    public function cancelReservation($rid = NULL) {

        $rid = (int)Sanitize::clean($rid);
        $hid = (int)Session::get('session-user')['hid'];

        // Verificar se o utilizador é o titular da reserva
        if($this->belongsToReservation($rid, $hid)) {

            // Apagar a reserva
            $query = 
            "DELETE FROM Reservations WHERE rid = '$rid'";

            $result = $this->getConnection()->query($query);

            if($result) {

                Session::set('cancel-success', 'A sua reserva foi cancelada.');

                return [
                    'status' => 200
                ];
            
            }

        }

        Session::set('cancel-error', 'De momento não é possível cancelar a sua reserva. Por favor, tente mais tarde ou entre em contato connosco.');

        return [
            'status' => 403
        ];

    }

    public function addGuests($data = []) {

        $rid = Sanitize::clean($data['rid']);
        $totalGuests = Sanitize::clean($data['totalGuests']);

        for($i = 0; $i < count($data['data']); $i++) {

            foreach($data['data'][$i] as $key => $value) {

                $value[key($value)] = Sanitize::clean($value[key($value)]);

                // Verificar se o hóspede não é o titular da reserva
                if(key($value) === 'email' && $value[key($value)] === Session::get('session-user')['email']) {

                    return [
                        'status' => 403
                    ];

                }

                // Verificar se o hóspede já se encontra registado
                if(key($value) === 'email') {

                    $email = $value[key($value)];

                    $query = "SELECT hid, email FROM Guests WHERE email = '$email' LIMIT 1";

                    $result = $this->getConnection()->query($query);

                    $rows = [];

                    while($row = $result->fetch_assoc()) {
                        array_push($rows, $row);
                    }

                    $result->free();

                    if(count($rows) > 0) {

                        $hid = $rows[0]['hid'];

                        // Adicionar hóspede à reserva
                        $query = "INSERT INTO Guests_Reservations(hid, rid) VALUES('$hid', '$rid')";

                        if(!$this->getConnection()->query($query)) {

                            return [
                                'status' => 403
                            ];

                        }

                    } else {

                        $name = $data['data'][$i][0]['name'];
                        $email = $data['data'][$i][1]['email'];
                        $dob = $data['data'][$i][2]['dob'];
                        $nationality = $data['data'][$i][3]['nationality'];
                        $country = $data['data'][$i][4]['country'];
                        $city = $data['data'][$i][5]['city'];
                        $cc = $data['data'][$i][6]['cc'];
                        $passport = $data['data'][$i][7]['passport'];
                        $password = password_hash(ShortCode::get(), PASSWORD_BCRYPT);

                        // Criar novo hóspede
                        $query = 
                        "INSERT INTO Guests(nome, data_nascimento, email, nacionalidade, país, cidade, password, cartão_cidadão, passaporte)
                        VALUES('$name', '$dob', '$email', '$nationality', '$country', '$city', '$password', '$cc', '$passport')";

                        $result =  $this->getConnection()->query($query);

                        if($result) {

                            // Adicionar hóspede à reserva
                            $query = "SELECT hid, email FROM Guests WHERE email = '$email' LIMIT 1";

                            $result = $this->getConnection()->query($query);

                            $rows = [];

                            while($row = $result->fetch_assoc()) {
                                array_push($rows, $row);
                            }

                            $result->free();

                            if(count($rows) > 0) {

                                $hid = $rows[0]['hid'];

                                $query = "INSERT INTO Guests_Reservations(hid, rid) VALUES('$hid', '$rid')";

                                if(!$this->getConnection()->query($query)) {

                                    return [
                                        'status' => 403
                                    ];

                                }

                            }

                        } else {

                            return [
                                'status' => 403
                            ];

                        }

                    }

                }

            }

        }

        Session::set("reservation-success", "Reserva registada com sucesso!");

        return [
            'status' => 200,
            'data' => 'ok!'
        ];

    }

}