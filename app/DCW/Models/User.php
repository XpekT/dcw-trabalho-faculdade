<?php

namespace DCW\Models;

use DCW\Database;
use DCW\Helpers\Sanitize;
use DCW\Classes\Mail;
use DCW\Classes\Session;
use ShortCode\Random as ShortCode;

if(session_status() === PHP_SESSION_NONE) {
    Session::start();
}

class User extends Database {

    public function register($data = []) {

        // Sanitanizar os dados
        $sanitizedData = Sanitize::clean($data);

        // Verifica se existe algum utilizador com o mesmo email
        $email = $sanitizedData['email'];
        
        if(self::isUserRegistered($email)) {

            return [
                "status" => 403,
                "error" => "Já existe um utilizador registado com este email!"
            ];

        } else {

            // Hasha a palavra-passe
            $sanitizedData['password'] = password_hash($sanitizedData['password'], PASSWORD_BCRYPT);
    
            // Cria um novo registo
            $name = $sanitizedData['name'];
            $dob = $sanitizedData['dob'];
            $nationality = $sanitizedData['nationality'];
            $country = $sanitizedData['country'];
            $city = $sanitizedData['city'];
            $password = $sanitizedData['password'];
            $cc = $sanitizedData['cc'];
            $passport = $sanitizedData['passport'];

            $query = 
            "INSERT INTO Guests (nome, data_nascimento, email, nacionalidade, país, cidade, password, cartão_cidadão, passaporte) 
            VALUES ('$name', '$dob', '$email', '$nationality', '$country', '$city', '$password', '$cc', '$passport')";

            $result = $this->getConnection()->query($query);

            // Enviar um mail para o utilizador com um link que ativa a conta
            $mail = new Mail();

            $mail->createMessage(
                'Confirmação de Registo - Yes We Rent',
                'admin@yeswerent.dx.am',
                $email,
                '<h1>Ativação de Conta!</h1><br><p>Clique no link seguinte para ativar a sua conta:</p><br><a href="' . $this->env["development"] . '/app/DCW/Controllers/activate.php?email=' . $email . '">Ativar!</a>'
            );

            try {

                if($result && $mail->sendMessage()) {

                    return [
                        "status" => 200,
                        "message" => "Conta registada! Verifique o seu email para ativar a sua conta."
                    ];

                }

            } catch (\Exception $e) {

                /* Eliminar o utilizador na base de dados se alguma coisa falhar no processo de registo */
                $query = "DELETE FROM Guests WHERE email = '$email'";
                $this->getConnection()->query($query);

                return [
                    "status" => 403,
                    "error" => "Alguma coisa correu mal. Por favor, tente mais tarde."
                ];

            }

        }

    }

    public function activate($email = NULL) {

        // Verificar se o utilizador se encontra registado
        $query = "SELECT hid, nome, data_nascimento, email, nacionalidade, país, cidade, cartão_cidadão, passaporte, ativo, adicionado_a FROM Guests WHERE email = '$email'";
        $result = $this->getConnection()->query($query);

        $rows = [];

        if($result) {
            
            // Agrega as linhas num array
            while ($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }

            // Liberta o conjunto
            $result->free();

        }

        if(count($rows) !== 0 && (int)$rows[0]['ativo'] === 0) {

            // Ativar o utilizador
            $query = "UPDATE Guests SET ativo = 1 WHERE email = '$email'";
            $result = $this->getConnection()->query($query);

            if($result) {
                
                // Ativação bem sucedida
                Session::set("activation-success", "Conta ativada com sucesso!");
                
                // Iniciar sessão do utilizador
                Session::set('session-user', $rows[0]);

                $hid = $rows[0]['hid'];

                return [
                    'status' => True,
                    'hid' => $hid
                ];

            } else {
            
                // Não existe nenhum utilizador com o email introduzido
                Session::set("activation-error", "Alguma coisa correu mal! Por favor, tente mais tarde.");

                // Redirecionar para página principal
                return [
                    'status' => False,
                ];

            }
            

        } else {

            // Não existe nenhum utilizador com o email introduzido - redireciona para página principal
            Session::set("activation-error", "Não existe nenhum utilizador registado com o email '$email' ou a conta já se encontra ativa.");

            // Redirecionar para página principal
            return [
                'status' => False,
            ];

        }

    }

    public function login($data = []) {

        // Sanitanizar os dados
        $sanitizedData = Sanitize::clean($data);

        // Verifica se existe algum utilizador com o mesmo email
        $email = $sanitizedData['email'];

        $query = $query = "SELECT hid, nome, data_nascimento, email, nacionalidade, país, cidade, password, cartão_cidadão, passaporte, ativo, adicionado_a FROM Guests WHERE email = '$email'";
        $result = $this->getConnection()->query($query);
        
        $rows = [];

        if($result) {
            
            // Agrega as linhas num array
            while ($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }

            // Liberta o conjunto
            $result->free();

        }

        // utilizador existe
        if(count($rows) > 0) {

            // Verificar se o utilizador tem a conta ativada
            if((int)$rows[0]['ativo'] !== 1) {
                
                return [
                    "status" => 403,
                    "error" => "Tem de ativar a sua conta! Verifique o seu email."
                ];

            }
            // Verificar se a password está correta
            elseif(password_verify($sanitizedData['password'], $rows[0]['password'])) {

                // Retirar o password do array antes de iniciar a sessão
                $rows[0]['password'] = '';

                // Iniciar sessão
                Session::set('session-user', $rows[0]);

                return [
                    "status" => 200,
                    "hid" => $rows[0]['hid']
                ];

            } else {

                return [
                    "status" => 403,
                    "error" => "As credenciais não estão corretas!"
                ];

            }

        } else {

            return [
                "status" => 403,
                "error" => "Não existe nenhum utilizador com o email '$email'."
            ];

        }

    }

    public function logout() {

        $userSession = Session::get('session-user');

        if(isset($userSession)) {
            
            Session::clear('session-user');
        
            return true;
        }

    }

    public function reservations($hid) {

        // Limpar o parâmetro
        $hid = Sanitize::clean($hid);

        $query = 
        "SELECT Guests.nome, Reservations.* FROM Guests_Reservations
        INNER JOIN Reservations ON Reservations.rid = Guests_Reservations.rid AND Guests_Reservations.hid = '$hid'
        INNER JOIN Guests ON Guests.hid = Reservations.titular
        ORDER BY Reservations.adicionado_a DESC";

        $result = $this->getConnection()->query($query);

        $rows = [];

        if($result) {

            while ($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }

            // Liberta o conjunto
            $result->free();

        } 

        return $rows;

    }

    public function getLastUserRegistered() {

        $query = "SELECT * FROM Guests ORDER BY hid DESC LIMIT 1";

        $result = $this->getConnection()->query($query);

        $rows = [];

        if($result) {

            // Agrega as linhas num array
            while ($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }

            // Liberta o conjunto
            $result->free();
        
        }

        return $rows[0];

    }

    public function isUserRegistered($email = NULL) {

        $query = "SELECT * FROM Guests WHERE email = '$email' LIMIT 1";
        
        $result = $this->getConnection()->query($query);
        
        $rows = [];
        
        if($result) {
        
                while($row = $result->fetch_assoc()) {
                        array_push($rows, $row);
                }
                
                $result->free();
        
        }

        return count($rows) > 0;

    }

    public function getUser($email = NULL) {

        $query = "SELECT * FROM Guests WHERE email = '$email'";

        $result = $this->getConnection()->query($query);

        $rows = [];

        if($result) {

            // Agrega as linhas num array
            while ($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }

            // Liberta o conjunto
            $result->free();
        
        }

        return $rows[0];

    }

    public function create($data = []) {

        // Sanitanizar os dados
        $sanitizedData = Sanitize::clean($data);

        $name = $sanitizedData['name'];
        $dob = $sanitizedData['dob'];
        $email = $sanitizedData['email'];
        $nationality = $sanitizedData['nationality'];
        $country = $sanitizedData['country'];
        $city = $sanitizedData['city'];
        $password = password_hash($sanitizedData['password'], PASSWORD_BCRYPT);
        $cc = $sanitizedData['cc'];
        $passport = $sanitizedData['passport'];

        $query = 
        "INSERT INTO Guests (nome, data_nascimento, email, nacionalidade, país, cidade, password, cartão_cidadão, passaporte, ativo, adicionado_a) 
        VALUES ('$name', '$dob', '$email', '$nationality', '$country', '$city', '$password', '$cc', '$passport', '0', NOW())";

        return $this->getConnection()->query($query);
        

    }

    public function delete($hid = NULL) {

        $hid = Sanitize::clean($hid);

        // Verificar se o utilizador detém a conta a ser eliminada
        if(Session::get('session-user')['hid'] === $hid) {

            // Apagar a conta do utilizador
            $query = "DELETE FROM Guests WHERE hid = '$hid'";

            $result = $this->getConnection()->query($query);

            if($result) {

                // Informar o utilizador
                Session::set('account-delete-success', 'A sua conta foi eliminada com sucesso!');
                
                // limpar a sessão do utilizador
                Session::clear('session-user');

                return [
                    'status' => 200
                ];
            
            }

        }
        
        Session::set('account-delete-error', 'De momento não é possível processar o seu pedido.');

        return [
            'status' => 403
        ];
    
    }

    public function edit($hid = NULL, $data = NULL) {

        $hid = Sanitize::clean($hid);
        $name = Sanitize::clean($data[0]['value']);
        $dob = Sanitize::clean($data[1]['value']);
        $email = Sanitize::clean($data[2]['value']);
        $nationality = Sanitize::clean($data[3]['value']);
        $country = Sanitize::clean($data[4]['value']);
        $city = Sanitize::clean($data[5]['value']);
        $cc = Sanitize::clean($data[6]['value']);
        $passport = Sanitize::clean($data[7]['value']);
        $password = Sanitize::clean($data[8]['value']);
        $password_confirmation = Sanitize::clean($data[9]['value']);

        $userSession = Session::get('session-user');

        // Verificar se o utilizador detém o perfil a ser editado
        if($userSession['hid'] === $hid) {

            $result = False;
        
            if(mb_strlen($name) !== 0 && $name !== $userSession['nome']) {

                $query = "UPDATE Guests SET nome = '$name' WHERE hid = '$hid'";
                $result = $this->getConnection()->query($query);

                if($result) {
                    Session::setField('session-user', 'nome', $name);
                }

            }

            if(mb_strlen($dob) !== 0 && $dob !== $userSession['data_nascimento']) {

                $query = "UPDATE Guests SET data_nascimento = '$dob' WHERE hid = '$hid'";
                $result = $this->getConnection()->query($query);

                if($result) {
                    Session::setField('session-user', 'data_nascimento', $dob);
                }

            }

            if(mb_strlen($email) !== 0 && $email !== $userSession['email']) {

                $query = "UPDATE Guests SET email = '$email' WHERE hid = '$hid'";
                $result = $this->getConnection()->query($query);

                if($result) {
                    Session::setField('session-user', 'email', $email);
                }

            }

            if(mb_strlen($nationality) !== 0 && $nationality !== $userSession['nacionalidade']) {

                $query = "UPDATE Guests SET nacionalidade = '$nationality' WHERE hid = '$hid'";
                $result = $this->getConnection()->query($query);

                if($result) {
                    Session::setField('session-user', 'nacionalidade', $nationality);
                }

            }

            if(mb_strlen($country) !== 0 && $country !== $userSession['país']) {

                $query = "UPDATE Guests SET país = '$country' WHERE hid = '$hid'";
                $result = $this->getConnection()->query($query);

                if($result) {
                    Session::setField('session-user', 'país', $country);
                }

            }

            if(mb_strlen($city) !== 0 && $city !== $userSession['cidade']) {

                $query = "UPDATE Guests SET cidade = '$city' WHERE hid = '$hid'";
                $result = $this->getConnection()->query($query);

                if($result) {
                    Session::setField('session-user', 'cidade', $city);
                }

            }

            if($cc !== $userSession['cartão_cidadão']) {

                $query = "UPDATE Guests SET cartão_cidadão = '$cc' WHERE hid = '$hid'";
                $result = $this->getConnection()->query($query);

                if($result) {
                    Session::setField('session-user', 'cartão_cidadão', $cc);
                }

            }

            if($passport !== $userSession['passaporte']) {

                $query = "UPDATE Guests SET passaporte = '$passport' WHERE hid = '$hid'";
                $result = $this->getConnection()->query($query);

                if($result) {
                    Session::setField('session-user', 'passaporte', $passport);
                }

            }

            if(mb_strlen($password) !== 0 && $password === $password_confirmation) {

                $hash = password_hash($password, PASSWORD_BCRYPT);

                $query = "UPDATE Guests SET password = '$hash' WHERE hid = '$hid'";
                $result = $this->getConnection()->query($query);

                // Obrigar o utilizador a iniciar uma nova sessão se mudar de password
                if($result) {
                    Session::clear('session-user');
                    Session::set('account-edit-success', 'Por favor, inicie novamente a sessão com a nova password.');
                }

            }

            if($result === True) {

                Session::set('account-edit-success', 'Perfil atualizado com sucesso');

                return [
                    'status' => 200
                ];
            
            } else {

                return [
                    'status' => 403,
                    'error' => 'Nenhum dado alterado!'
                ];

            }
        
        }

        return [
            'status' => 403,
            'redirect' => True
        ];

    }

    public function resetPassword($email = NULL) {

        $email = Sanitize::clean($email);

        // Verificar se o utilizador está registado
        $query = "SELECT * FROM Guests WHERE email='$email' LIMIT 1";
        $result = $this->getConnection()->query($query);

        $rows = [];

        if($result) {

            while($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }

            $result->free();

        }

        if(count($rows) > 0) {

            // Gerar um password aleatório
            $newPassword = ShortCode::get();
            $newHash = password_hash($newPassword, PASSWORD_BCRYPT);

            // Enviar email com a nova password
            $mail = new Mail();

            $mail->createMessage(
                'Reposição de Password - Yes We Rent',
                'admin@yeswerent.dx.am',
                $email,
                '<h1>Nova Password!</h1><br><p>Esta é a password gerada aleatóriamente: ' . $newPassword . '</p><br><p style="color: red;"><strong>Mude esta password assim que aceder à sua conta para uma à sua escolha!</strong></p>'
            );

            try {

                if($mail->sendMessage()) {

                    // Atualizar na BD
                    $query = "UPDATE Guests SET password = '$newHash' WHERE email = '$email'";

                    if($this->getConnection()->query($query)) {
                        
                        return [
                            'status' => 200,
                            'message' => 'Password reposta! Verifique o seu email.'
                        ];
                    
                    }

                }

            } catch (\Exception $e) {

                return [
                    "status" => 403,
                    "error" => "Alguma coisa correu mal. Por favor, tente mais tarde."
                ];

            }
        
        } else {

            return [
                'status' => 403,
                'error' => 'Não existe nenhum utilizador registado com o email ' . $email . '!'
            ];

        } 

    }

    public function resendActivation($email = NULL) {

        $email = Sanitize::clean($email);

        // Verificar se o utilizador está registado
        $query = "SELECT * FROM Guests WHERE email='$email' LIMIT 1";
        $result = $this->getConnection()->query($query);

        $rows = [];

        if($result) {

            while($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }

            $result->free();

        }

        // Verificar se a conta do utilizador já está ativada
        if((int)$rows[0]['ativo'] === 1) {

            return [
                "status" => 403,
                "error" => "A sua conta já se encontra ativa!"
            ];

        } else {

            if(count($rows) > 0) {
    
                // Enviar um mail para o utilizador com um link que ativa a conta
                $mail = new Mail();
    
                $mail->createMessage(
                    'Confirmação de Registo - Yes We Rent',
                    'admin@yeswerent.dx.am',
                    $email,
                    '<h1>Ativação de Conta!</h1><br><p>Clique no link seguinte para ativar a sua conta:</p><br><a href="' . $this->env["development"] . '/app/DCW/Controllers/activate.php?email=' . $email . '">Ativar!</a>'
                );
     
                try {
    
                    if($result && $mail->sendMessage()) {
    
                        return [
                            "status" => 200,
                            "message" => "Verifique o seu email para ativar a sua conta."
                        ];
    
                    }
    
                } catch (\Exception $e) {
    
                    return [
                        "status" => 403,
                        "error" => "Alguma coisa correu mal. Por favor, tente mais tarde."
                    ];
    
                }
            
            } else {
    
                return [
                    'status' => 403,
                    'error' => 'Não existe nenhum utilizador registado com o email ' . $email . '!'
                ];
    
            }
        
        }

    }

}