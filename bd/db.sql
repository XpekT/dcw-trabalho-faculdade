/* Elimina a BD 'dcw' se existir */
DROP DATABASE IF EXISTS dcw;

/* Criar BD se não existir */
CREATE DATABASE IF NOT EXISTS dcw;

/* Usar a BD 'dcw' */
USE dcw;

/* Criar a tabela Guests */
CREATE TABLE Guests (
    hid INTEGER AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR(30) NOT NULL,
    data_nascimento VARCHAR(10),
    email VARCHAR(100) NOT NULL UNIQUE,
    nacionalidade VARCHAR(50) NOT NULL,
    país VARCHAR(50) NOT NULL,
    cidade VARCHAR(50) NOT NULL,
    password VARCHAR(255) NOT NULL,
    cartão_cidadão VARCHAR(15) DEFAULT NULL,
    passaporte VARCHAR(15) DEFAULT NULL,
    ativo TINYINT(1) DEFAULT 0,
    adicionado_a DATETIME NOT NULL DEFAULT NOW()
);

/* Criar a tabela Properties */
CREATE TABLE Properties (
    iid INTEGER AUTO_INCREMENT PRIMARY KEY,
    título TEXT(20),
    tipologia VARCHAR(6) NOT NULL,
    lotação TINYINT(10) NOT NULL,
    crianças TINYINT(1) NOT NULL,
    animais TINYINT(1) NOT NULL,
    preço_diário DECIMAL(8, 2) NOT NULL,
    descrição TEXT(1000) NOT NULL,
    foto_principal VARCHAR(255),
    foto_secundária_1 VARCHAR(255),
    foto_secundária_2 VARCHAR(255),
    foto_secundária_3 VARCHAR(255),
    foto_secundária_4 VARCHAR(255),
    cidade VARCHAR(50),
    localização VARCHAR(255) NOT NULL,
    adicionado_a DATETIME NOT NULL DEFAULT NOW()
);

/* Criar a tabela Reservations */
CREATE TABLE Reservations (
    rid INTEGER AUTO_INCREMENT PRIMARY KEY,
    data_entrada VARCHAR(10) NOT NULL,
    data_saída VARCHAR(10) NOT NULL,
    valor_total DECIMAL(10, 2),
    hóspedes_adicionais TINYINT(10) NOT NULL DEFAULT 0,
    código VARCHAR(255),
    adicionado_a DATETIME NOT NULL DEFAULT NOW(),
    iid INTEGER,
    titular INTEGER,
    FOREIGN KEY(iid) REFERENCES Properties(iid) ON DELETE CASCADE,
    FOREIGN KEY(titular) REFERENCES Guests(hid) ON DELETE CASCADE
);

/* Criar a tabela Guests_Reservas */
CREATE TABLE Guests_Reservations (
    grid INTEGER AUTO_INCREMENT PRIMARY KEY,
    hid INTEGER,
    rid INTEGER,
    FOREIGN KEY(hid) REFERENCES Guests(hid) ON DELETE CASCADE,
    FOREIGN KEY(rid) REFERENCES Reservations(rid) ON DELETE CASCADE
);