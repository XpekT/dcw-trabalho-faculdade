<?php

require "vendor/autoload.php";

use DCW\Classes\Session;

Session::start();

$countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");

if(isset($_GET['rid']) && isset($_GET['totalGuests'])) {
    
    $hid = $_SESSION['session-user']['hid'];
    $rid = $_GET['rid'];
    $totalGuests = $_GET['totalGuests'];

} else {

    header('Location: /');

    exit();

}

?>

<!doctype html>
<html class="no-js" lang="pt_PT">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>DCW - Trabalho 1</title>
		<meta name="description" content="Trabalho Prático 1">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="icon" type="image/x-icon" href="../favicon.ico" />
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
		<link rel="stylesheet" href="css/normalize.css">
		<link rel="stylesheet" href="js/jquery-ui-1.12.1.custom/jquery-ui.min.css">
		<link rel="stylesheet" href="js/jquery-ui-1.12.1.custom/jquery-ui.theme.min.css">
		<link rel="stylesheet" href="js/jquery-ui-1.12.1.custom/jquery-ui.structure.min.css">
		<link rel="stylesheet" href="css/main.css">
	</head>

    <body>

        <div class="guests-form-wrapper">

            <div class="header-title">
                <h1><i class="fas fa-info-circle"></i> Introduza os dados para os restantes hóspedes</h1>
            </div>

            <form id="guests-form" data-rid=<?php echo $rid; ?> data-hid=<?php echo $hid; ?>>
                <div class="inputs-wrapper">
                    <?php for($i = 0; $i < $totalGuests; $i++): ?>
                        <div class="guest-wrapper">
                            <h3>Hóspede #<?php echo $i + 1; ?></h3>
                            <div class="input-wrapper">
                                <label for=<?php echo "name-'$i'"; ?>>Nome</label>
                                <input type="text" autocomplete="off" name=<?php echo "name-'$i'"; ?> id=<?php echo "name-'$i'"; ?>>
                            </div>
                            <div class="input-wrapper">
                                <label for=<?php echo "email-'$i'"; ?>>Email</label>
                                <input type="email autocomplete="off"" name=<?php echo "email-'$i'"; ?> id=<?php echo "email-'$i'"; ?>>
                            </div>
                            <div class="input-wrapper">
                                <label for=<?php echo "dob-'$i'"; ?>>Data Nascimento</label>
                                <input type="text" autocomplete="off" name=<?php echo "dob-'$i'"; ?> id=<?php echo "dob-'$i'"; ?> placeholder="Por ex. 24-01-1987 ou 01-24-1987">
                            </div>
                            <div class="input-wrapper">
                                <label for=<?php echo "nationality-'$i'"; ?>>Nacionalidade</label>
                                <select name=<?php echo "nationality-'$i'"; ?> id=<?php echo "nationality-'$i'"; ?>>
                                    <?php  foreach($countries as $country): ?>
                                        <?php echo '<option value='.$country.'>'.$country.'</option>'; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="input-wrapper">
                                <label for=<?php echo "country-'$i'"; ?>>País</label>
                                <select name=<?php echo "country-'$i'"; ?> id=<?php echo "country-'$i'"; ?>>
                                    <?php  foreach($countries as $country): ?>
                                        <?php echo '<option value='.$country.'>'.$country.'</option>'; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="input-wrapper">
                                <label for=<?php echo "city-'$i'"; ?>>Cidade</label>
                                <input type="text" autocomplete="off" name=<?php echo "city-'$i'"; ?> id=<?php echo "city-'$i'"; ?>>
                            </div>
                            <div class="input-wrapper">
                                <label for=<?php echo "cc-'$i'"; ?>>Cartão Cidadão</label>
                                <input type="text" autocomplete="off" name=<?php echo "cc-'$i'"; ?> id=<?php echo "cc-'$i'"; ?>>
                            </div>
                            <div class="input-wrapper">
                                <label for=<?php echo "passport-'$i'"; ?>>Passaporte</label>
                                <input type="text" autocomplete="off" name=<?php echo "passport-'$i'"; ?> id=<?php echo "passport-'$i'"; ?>>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>
                <input id="totalGuests" type="hidden" name="total" value=<?php echo $totalGuests; ?>>
                <div id="guests-form-messages-wrapper"></div>
                <div class="buttons-wrapper">
                    <button type="submit">Confirmar!</button>
                    <button type="reset">Limpar!</button>
                    <button type="button" id="cancel-reservation-button">Cancelar Reserva!</button>
                </div>
            </form>
        </div>

        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/moment.min.js"></script>

        <script type="text/javascript">
        
            let rid = $('#guests-form').attr('data-rid')
            let hid = $('#guests-form').attr('data-hid')

            let formatedData = []
            let totalGuests = $('#totalGuests').val()
            let messageWrapper = $('#guests-form-messages-wrapper')

            function cancelOperation(message) {
                messageWrapper.html('<p class="error"><i class="fas fa-info-circle"></i> ' + message + '</p>')
            }

            function cancelReservation() {

                let proceedWithCancellation = confirm('Tem a certeza que quer cancelar a reseva?')

                if(proceedWithCancellation) {
                    
                    $.ajax({
                        url: '/app/DCW/Controllers/cancel.php',
                        type: 'POST',
                        data: { rid },
                        dataType: 'json'
                    })
                    .done(function(data) {
                        window.location = '/profile.php?hid=' + hid
                    })

                }

            }

            $('#guests-form').on('submit', function(event) {

                event.preventDefault()
                
                formatedData = []

                let data = []
                let group = []
                
                let hasErrors = false
                let dateHolder = null
                let emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/

                messageWrapper.html('')

                // Arranjar os dados
                $(this).serializeArray().forEach((value, index) => {

                    if(index === 8 || index === 16 || index === 24 || index === 32 || index === 40 || index === 48) {
                        
                        data.push(group)
                        group = []
                        group.push(value)

                    } else {

                        group.push(value)
                    
                    } 


                })
                
                for(let i = 0; i < totalGuests; i++) {

                    let temporary = []

                    if(hasErrors) {
                        break
                    }

                    for(let j = 0; j < data[i].length; j++){

                        let name = data[i][j].name.split('-')[0]
                        let value = data[i][j].value

                        if(value.length !== 0) {
                        
                            if(name === 'dob') {

                                dateHolder = moment(value, ["MM-DD-YYYY", "YYYY-MM-DD", "MM/DD/YYYY", "DD/MM/YYYY"])

                                if(moment(dateHolder).isValid()) {

                                    temporary[j] = { [name]: dateHolder._i }
                                    hasErrors = false
                                
                                } else {
                                    cancelOperation('Alguma data que introduziu não é válida!')
                                    hasErrors = true
                                    formatedData = []
                                    break
                                }                        
                            
                            } else if(name === 'email') {

                                if(emailRegex.test(value)) {

                                    temporary[j] = { [name]: value }
                                
                                } else {
                                    cancelOperation('Algum dos emails introduzido não é válido!')
                                    hasErrors = true
                                    formatedData = []
                                    break
                                }

                            } else {
                                temporary[j] = { [name]: value }
                            }
                        
                        } else if(name === 'cc') {

                            if(data[i][j + 1].value.length === 0) {
                                cancelOperation('Tem de fornecer um método de identificação.')
                                hasErrors = true
                                formatedData = []
                                break
                            } else {
                                temporary[j] = { [name]: value }
                            }

                        } else if(name === 'passport') {

                            if(data[i][j - 1].value.length === 0) {
                                cancelOperation('Tem de fornecer um método de identificação.')
                                hasErrors = true
                                formatedData = []
                                break
                            } else {
                                temporary[j] = { [name]: value }
                            }

                        } else {
                            cancelOperation('Não preencheu o algum do campo do formulário!')
                            hasErrors = true
                            formatedData = []
                            break
                        }

                    }

                    if(!hasErrors) {
                        formatedData.push(temporary)
                    }

                }

                // Adicionar os hóspedes à reserva
                if(!hasErrors) {
                    
                    $.ajax({
                        url: '/app/DCW/Controllers/guests.php',
                        type: 'POST',
                        data: { rid: rid, data: formatedData, totalGuests: totalGuests },
                        dataType: 'json'
                    })
                    .done(function(data) {

                        if(data.status === 200) {

                            window.location = '/profile.php?hid=' + hid

                        } 

                        if(data.status === 403) {
                            cancelReservation()
                        }

                    })

                } else {
                    formatedData = []
                }

            })

            // Cancelamento da reserva
            $('#cancel-reservation-button').on('click', cancelReservation)

        </script>

    </body>

</html>
