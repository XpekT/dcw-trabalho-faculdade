<?php

require "vendor/autoload.php";

use DCW\Classes\Session;

if(session_status() === PHP_SESSION_NONE) {
    Session::start();
}

// Verificar se utilizador tem sessão iniciada
$userSession = isset($_SESSION['session-user']) ? $_SESSION['session-user'] : NULL;

?>

<div id="blocker">
    <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
    <h1>A Carregar...</h1>
</div>
<footer>
    <div class="sitemap">
        <h3><i class="fas fa-map"></i> Mapa do Site</h3>
        <ul>
            <li>
                <a href="/">Home</a>
            </li>
            <?php if(!$userSession): ?>
                <li>
                    <a href="/accounts.php">Entrar/Registar</a>
                </li>
            <?php endif; ?>
            <?php if($userSession): ?>
                <li>
                    <a href=<?php echo "/profile.php?=hid" . $userSession['hid'] . "" ?>>Perfil</a>
                </li>
            <?php endif; ?>
            <li>
                <a href="#info">Informações</a>
            </li>
            <li>
                <a href="/offers.php">Ofertas</a>
            </li>
            <?php if($userSession): ?>
                <li>
                    <a href="#" class="signout">Sair!</a>
                </li>
            <?php endif; ?>
        </ul>
    </div>
    <div class="googlemaps">
        <h3><i class="fas fa-map-marker-alt"></i> Localização</h3>
        <iframe scrolling="no" title="Google Maps" aria-label="Google Maps" src="https://static.parastorage.com/services/santa/1.5523.4/static/external/googleMap.html?language=pt-BR&amp;lat=41.151&amp;long=-8.59753390000003&amp;address=Av. de Fernão de Magalhães 98, 4300 Porto, Portugal&amp;addressInfo=Yes We Rent! - Alojamento Local&amp;showZoom=true&amp;showStreetView=true&amp;showMapType=true" width="100%" height="100%" frameborder="0"></iframe>
        <p><strong>Morada - </strong> Rua de Cima, nº 202 Avenida da Boavista - Porto</p>
        <p><strong> GPS - </strong> 32°42′54″ N, 117°9′45″ W</p>
        <p><strong>Atendimento - </strong> Segunda a Sábado das 09h00 às 13h00 e das 14h00 às 19h00</p>
    </div>
    <div class="disclaimer">
        <h3><i class="fas fa-info-circle"></i> Disclaimer</h3>
        <p>Yes We Rent! &copy; Todos os direitos reservados - 2018</p>
    </div>
</footer>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/moment.min.js"></script>
<script type="text/javascript" src="js/fuse.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>

<script type="text/javascript">

    $(function() {

        $('#blocker').fadeOut('slow')

        // Limpar as sessões ao clique dos utilizadores
        $(".session-wrapper .close").on("click", function() {

            let type = $(this).attr('data-type')

            $.ajax({
                url: '/app/DCW/Controllers/clear.php',
                type: 'POST',
                data: { type },
                dataType: 'json'
            })
            .done(function(data) {

                if(data.cleared) {
                    $(".session-wrapper").slideUp(function() {
                        $(".session-wrapper").remove()
                    })
                }

            })

        })

        // Logout
        $('.signout').on('click', function() {

            $.ajax({
                url: '/app/DCW/Controllers/logout.php',
                type: 'POST',
                data: { logout: true },
                dataType: 'json'
            })
            .done(function(data) {

                if(data.status === 200) {
                    window.location.href = '/';
                }

            })

        })
    
    })


</script>