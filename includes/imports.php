<?php

require "vendor/autoload.php";

use DCW\Classes\Session;

$ativationSuccessMessage = Session::get('activation-success');
$ativationErrorMessage = Session::get('activation-error');
$ativationInfoMessage = Session::get('activation-info');

$reservationSuccessMessage = Session::get('reservation-success');
$cancelReservationSuccessMessage = Session::get('cancel-success');
$cancelReservationErrorMessage = Session::get('cancel-error');

$deleteAccountSuccessMessage = Session::get('account-delete-success');
$deleteAccountErrorMessage = Session::get('account-delete-error');

$editAccountSuccessMessage = Session::get('account-edit-success');

?>

<!doctype html>
<html class="no-js" lang="pt_PT">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>DCW - Trabalho 1</title>
		<meta name="description" content="Trabalho Prático 1">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="icon" type="image/x-icon" href="../favicon.ico" />
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
		<link rel="stylesheet" href="css/normalize.css">
		<link rel="stylesheet" href="js/jquery-ui-1.12.1.custom/jquery-ui.min.css">
		<link rel="stylesheet" href="js/jquery-ui-1.12.1.custom/jquery-ui.theme.min.css">
		<link rel="stylesheet" href="js/jquery-ui-1.12.1.custom/jquery-ui.structure.min.css">
		<link rel="stylesheet" href="css/main.css">
	</head>

	<body>
		<?php if ($ativationSuccessMessage): ?>
			<div class="session-wrapper success">
				<p><?php echo $ativationSuccessMessage;  ?></p>
				<i class="fas fa-times close" data-type="activation-success"></i>
			</div>
		<?php elseif ($ativationErrorMessage): ?>
			<div class="session-wrapper error">
				<p><i class="fas fa-exclamation-circle"></i> <?php echo $ativationErrorMessage;  ?></p>
				<i class="fas fa-times close" data-type="activation-error"></i>
			</div>
		<?php elseif ($ativationInfoMessage): ?>
			<div class="session-wrapper info">
				<p class="info"><i class="fas fa-exclamation-circle"></i> <?php echo $ativationInfoMessage;  ?></p>
				<i class="fas fa-times close" data-type="activation-info"></i>
			</div>
		<?php elseif ($reservationSuccessMessage): ?>
			<div class="session-wrapper success">
				<p class="success"><i class="fas fa-check-circle"></i> <?php echo $reservationSuccessMessage;  ?></p>
				<i class="fas fa-times close" data-type="reservation-success"></i>
			</div>
		<?php elseif ($cancelReservationSuccessMessage): ?>
			<div class="session-wrapper success">
				<p class="success"><i class="fas fa-check-circle"></i> <?php echo $cancelReservationSuccessMessage;  ?></p>
				<i class="fas fa-times close" data-type="cancel-success"></i>
			</div>
		<?php elseif ($deleteAccountSuccessMessage): ?>
			<div class="session-wrapper success">
				<p class="success"><i class="fas fa-check-circle"></i> <?php echo $deleteAccountSuccessMessage;  ?></p>
				<i class="fas fa-times close" data-type="account-delete-success"></i>
			</div>
		<?php elseif ($editAccountSuccessMessage): ?>
			<div class="session-wrapper success">
				<p class="success"><i class="fas fa-check-circle"></i> <?php echo $editAccountSuccessMessage;  ?></p>
				<i class="fas fa-times close" data-type="account-edit-success"></i>
			</div>
		<?php elseif ($cancelReservationErrorMessage): ?>
			<div class="session-wrapper error">
				<p class="error"><i class="fas fa-exclamation-circle"></i> <?php echo $cancelReservationErrorMessage;  ?></p>
				<i class="fas fa-times close" data-type="cancel-error"></i>
			</div>
		<?php elseif ($deleteAccountErrorMessage): ?>
			<div class="session-wrapper error">
				<p class="error"><i class="fas fa-exclamation-circle"></i> <?php echo $deleteAccountErrorMessage;  ?></p>
				<i class="fas fa-times close" data-type="account-delete-error"></i>
			</div>
		<?php endif; ?>

		<!-- Pesquisa -->
		<div id="search-wrapper">
			<div class="input-wrapper">
				<input type="search" name="search" id="search" placeholder="Procurar...">
				<i class="fas fa-times-circle" id="close-search"></i>
			</div>
			<div class="results-wrapper"></div>
			<div id="loading-and-state"></div>
		</div>