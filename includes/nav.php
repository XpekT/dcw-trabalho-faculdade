<?php

require "vendor/autoload.php";

use DCW\Classes\Session;

if(session_status() === PHP_SESSION_NONE) {
    Session::start();
}

// Verificar se utilizador tem sessão iniciada
$userSession = isset($_SESSION['session-user']) ? $_SESSION['session-user'] : NULL;

?>
<nav>
    <i class="fas fa-bars menu-icon" id="menu-icon"></i>
    <ul id="menu-list">
        <li>
            <a data-page="home" href="/"><i class="fas fa-home"></i> Home</a>
        </li>
        <?php if(!$userSession): ?>
            <li>
                <a data-page="accounts" href="/accounts.php"><i class="fas fa-key"></i> Entrar/Registar</a>
            </li>
        <?php endif; ?>
        <?php if($userSession): ?>
            <li>
                <a data-page="profile" href=<?php echo "/profile.php?=hid" . $userSession['hid'] . "" ?>><i class="fas fa-user"></i> Perfil</a>
            </li>
        <?php endif; ?>
        <li>
            <a href="/index.php#info"><i class="fas fa-question-circle"></i> Informações</a>
        </li>
        <li>
            <a data-page="offers" href="/offers.php"><i class="fas fa-gift"></i> Ofertas</a>
        </li>
        <li>
            <a href="#" id="open-search"><i class="fas fa-search"></i> Pesquisar</a>
        </li>
        <?php if($userSession): ?>
            <li>
                <i class="fas fa-sign-out-alt signout"></i>
            </li>
        <?php endif; ?>
    </ul>
</nav>