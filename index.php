<?php 

require "vendor/autoload.php";

use DCW\Classes\Session;

Session::start();

?>

<?php include_once(__DIR__ . '/includes/imports.php') ?>

		<?php include_once(__DIR__ . '/includes/nav.php') ?>

		<div class="main-container">

			<header class="main-header">
			  <div class="header-wrapper">
					<h1>Yes We Rent!</h1>
					<p>Alojamento Local - Grande Porto</p>
			  </div>
			</header>
			<div class="descriptions-wrapper">
			  <h2><i class="fas fa-concierge-bell"></i> Serviços Garantidos!</h2>
			  <div class="services-wrapper">
					<div class="service">
						<i class="fas fa-gift" style="color:lightpink;"></i>
						<h3>Carisma Portuense</h3>
						<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus reprehenderit corporis amet in assumenda impedit vel officia? Sunt laboriosam impedit, cumque dolorem quisquam cum dolor similique, recusandae accusantium nesciunt asperiores.</p> 
					</div>
					<div class="service">
						<i class="fas fa-concierge-bell" style="color: lightblue;"></i>
						<h3>Check-In</h3>
						<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus reprehenderit corporis amet in assumenda impedit vel officia? Sunt laboriosam impedit, cumque dolorem quisquam cum dolor similique, recusandae accusantium nesciunt asperiores.</p> 
					</div>
					<div class="service">
						<i class="fas fa-cogs" style="color: lightsalmon;"></i>
						<h3>Limpeza e Manutenção</h3>
						<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus reprehenderit corporis amet in assumenda impedit vel officia? Sunt laboriosam impedit, cumque dolorem quisquam cum dolor similique, recusandae accusantium nesciunt asperiores.</p>
					</div>
					<div class="service">
						<i class="fas fa-user-tie" style="color:lightseagreen;"></i>	
						<h3>Consultadoria e Decoração</h3>
						<p>Dignissimos eius atque doloribus, corrupti quis assumenda blanditiis, officia quas, sint ratione cupiditate reprehenderit! Asperiores corrupti voluptatum non nesciunt molestias tempora excepturi, tempore, quaerat nulla earum omnis, labore animi expedita.</p>  
					</div>
					<div class="service">
						<i class="fas fa-city" style="color:lightcoral;"></i>	
						<h3>Cancelamento na hora</h3>
						<p>Dignissimos eius atque doloribus, corrupti quis assumenda blanditiis, officia quas, sint ratione cupiditate reprehenderit! Asperiores corrupti voluptatum non nesciunt molestias tempora excepturi, tempore, quaerat nulla earum omnis, labore animi expedita.</p>  
					</div>
					<div class="service">
						<i class="fas fa-headset" style="color:lightsteelblue;"></i>	
						<h3>Suporte 24 Horas</h3>
						<p>Dignissimos eius atque doloribus, corrupti quis assumenda blanditiis, officia quas, sint ratione cupiditate reprehenderit! Asperiores corrupti voluptatum non nesciunt molestias tempora excepturi, tempore, quaerat nulla earum omnis, labore animi expedita.</p>  
					</div>
			  </div>
			</div>
			<div class="offers-intro">
				<h2><i class="fas fa-paperclip"></i> Temos as melhores ofertas de alojamento no Grande Porto!</h2>
				<div class="offers-img-wrapper">
					<!-- AQUI VÃO ALGUMAS OFERTAS DA BASE DE DADOS -->
					<img src="/images/sub1.jpg" alt="offer-img">
					<img class="tablet-plus-only" src="/images/sub2.jpg" alt="offer-img">
					<img class="tablet-plus-only" src="/images/sub3.jpg" alt="offer-img">
					<img class="tablet-plus-only" src="/images/sub4.jpg" alt="offer-img">
				</div>
				<div class="offers-intro-link-wrapper">
					<a href="/offers.php"><i class="fas fa-gift"></i> Ver Ofertas!</a>
				</div>
			</div>
			<div class="conditions-to-host">
				<h1>Terá todas as condições para uma excelente estadia.</h1>
				<div class="conditions-wrapper">
					<div class="service">
						<i class="fas fa-utensils" style="color:slategrey;"></i>
						<h3>Itens Básicos</h3>
						<p>Qualquer estadia requer utensílios como pratos, copos, talheres, vassoura, apanhador e balde do lixo.</p>
					</div>
					<div class="service">
						<i class="fas fa-space-shuttle" style="color: indianred;"></i>
						<h3>Cozinha Equipada</h3>
						<p>É fundamental ter fogão, microondas e frigorífico. Uma cafeteira e chaleira são aconselháveis.</p>
					</div>
					<div class="service">
						<i class="fas fa-bed" style="color: skyblue;"></i>
						<h3>Camas Comfortáveis</h3>
						<p>Uma noite bem dormida é fundamental para conseguir tirar partido das experiências do dia seguinte.</p>
					</div>
					<div class="service">
						<i class="fas fa-wifi" style="color:mediumseagreen;"></i>
						<h3>Wi-Fi</h3>
						<p>Mais do que uma televisão, ter internet disponível é um dos requisitos mais procurados pelos nossos hóspedes.</p>
					</div>
				</div>
			</div>
			<div class="informations" id="info">
				<h1>Tem alguma dúvida? Fale connosco!</h1>
				<form id="informations-form">
					<div class="input-wrapper">
						<label for="name"><i class="fas fa-user"></i> Nome</label>
						<input type="text" name="name" id="name">
					</div>
					<div class="input-wrapper">
						<label for="email"><i class="fas fa-at"></i> Email</label>
						<input type="email" name="email" id="email">
					</div>
					<div class="input-wrapper">
						<label for="contact"><i class="fas fa-phone"></i> Telefone</label>
						<input type="text" name="contact" id="contact">
					</div>
					<div class="input-wrapper">
						<label for="subject"><i class="fas fa-question-circle"></i> Dúvida</label>
						<select name="subject" id="subject">
							<option value="Preços">Preços</option>
							<option value="Reclamações">Reclamações</option>
							<option value="Áreas Disponíveis">Zonas Disponíveis</option>
							<option value="Cancelamentos">Cancelamentos</option>
						</select>
					</div>
					<div class="input-wrapper">
						<label for="message"><i class="fas fa-pen"></i> Mensagem</label>
						<textarea name="message" id="message" cols="30" rows="10" placeholder="Escreva aqui a sua mensagem! Tentaremos ser o mais breve possível na resposta."></textarea>
					</div>
					<div class="input-wrapper">
						<label for="captcha"><i class="fas fa-question-circle"></i> Captcha</label>
						<input id="captcha" type="text" name="captcha">
						<div class="captcha-image-wrapper">
							<img id="captcha-img" src="#" alt="captcha-image">
							<i id="update-captcha" class="fas fa-redo"></i>
						</div>
					</div>
					<div class="buttons-wrapper">
						<button type="submit" id="button-submit">Enviar!</button>
						<button type="reset" id="button-reset">Limpar!</button>
					</div>
					<div class="information-form-messages-wrapper"></div>
				</form>
			</div>
		
		</div>
		
		<?php include_once(__DIR__ . '/includes/footer.php'); ?>

		<script type="text/javascript">
		
			$(function() {

				let img = $('#captcha-img')

				function generateCaptcha() {

					$.ajax({
						cache: false,
						url: '/app/DCW/Controllers/captcha.php',
						type: 'POST',
						data: { generate: true },
						dataType: 'json'
					})
					.done(function(data) {

						if(data.status === 200) {

							img.attr('src', `/images/${data.imageName}.jpg`)

						}

					})

				}

				// Buscar o captcha para o formulário de pedido de informações
				generateCaptcha()

				// Atualizar o captcha
				$('#update-captcha').on('click', generateCaptcha)

				// Submissão do formulário de pedido de informações
				$('#informations-form').on('submit', function(event) {

					event.preventDefault()

					$('#button-submit').prop('disabled', 'disabled')

					let messageWrapper = $('.information-form-messages-wrapper')

					messageWrapper.html('<p>A processar a sua mensagem...</p>')

					// Reunir os dados
					let data = $(this).serializeArray()

					// Pedido AJAX
					$.ajax({
						url: '/app/DCW/Controllers/information.php',
						type: 'POST',
						data: data,
						dataType: 'json',
						beforeSend: function() {

							let isClean = false

							data.forEach(el => {
						
								if(el.value.trim().length === 0) {
									messageWrapper.html('<p class="error"><i class="fas fa-info-circle"></i> Tem de preencher todos os campos do formulário</p>')
									isClean = false
								} else {
									isClean = true
								}
							
							})
							
							isClean === false ? $('#button-submit').prop('disabled', '') : $('#button-submit').prop('disabled', 'disabled')

							return isClean

						}
					
					})
					.done(function(data) {

						generateCaptcha()

						$('#button-submit').prop('disabled', '')
						$('#button-reset').click()

						if(data.status === 200) {
							messageWrapper.html('<p class="success"><i class="fas fa-check-circle"></i> ' + data.message + '</p>')
						} else {
							messageWrapper.html('<p class="error"><i class="fas fa-info-circle"></i> ' + data.error + '</p>')
						}

					})

				})
			
			})

		</script>

	</body>

</html>