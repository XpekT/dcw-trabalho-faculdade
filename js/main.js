$(function() {

    let iid = $('#main-container').attr('data-iid')
    let rid = $('#main-container').attr('data-rid')
    let hid = $('#main-container').attr('data-hid')

    let property = null

    let finalPrice = 0

    // Entry Date Vars
    let formEntry = $("#entry-date-reservation-form")
    let messageWrapperEntry = $("#entry-date-reservation-form-message-wrapper")
    let spinnerEntry = $("#entry-date-reservation-spinner")
    let pickedEntryDate = $('#entry-date-datepicker-reservation').datepicker('getDate')
    let canProcessReservationEntry = false

    // Leave Date Vars
    let formLeave = $("#leave-date-reservation-form")
    let messageWrapperLeave = $("#leave-date-reservation-form-message-wrapper")
    let spinnerLeave = $("#leave-date-reservation-spinner")
    let pickedLeaveDate = $('#leave-date-datepicker-reservation').datepicker('getDate')
    let canProcessReservationLeave = false

    let makeReservationWrapper = $('#make-reservation-inputs-wrapper')
    makeReservationWrapper.hide()

    $('.control-group').controlgroup()

    let cancelDialog = $("#cancel-reservation-dialog-confirm").dialog({
        resizable: false,
        autoOpen: false,
        draggable: false,
        height: "auto",
        width: 400,
        modal: true,
        buttons: {
            Confirmar: cancelReservation,
            Cancelar: function() {
                $(this).dialog("close");
            }
        }
    })

    // Esconder o conteúdo até ter concluído a busca das datas disponíveis
    formEntry.hide()

    function entryVerification() {
        
        pickedEntryDate = $('#entry-date-datepicker-reservation').val()

        spinnerEntry.show()

        // Pedido AJAX ao backend
        $.ajax({
            url: '/app/DCW/Controllers/availability.php',
            type: 'POST',
            data: { pickedDate: pickedEntryDate, iid: iid },
            dataType: 'json'
        })
        .done(function(data) {

            spinnerEntry.hide()
            
            if(data.dates.length === 0) {
                
                canProcessReservationEntry = true
                messageWrapperEntry.html('<p class="success">Data Disponível!</p>')
            
            } else {
            
                canProcessReservationEntry = false
                messageWrapperEntry.html('<p class="error">Data Indisponível!</p>')

            }

        })
    }

    function entryConfirmation() {
        
        if(canProcessReservationEntry) {
            dialogLeave.dialog("open")
        }
   
    }

    function entryClose() {
        dialogEntry.dialog("close");
    }

    function leaveVerification() {

        pickedLeaveDate = $('#leave-date-datepicker-reservation').val()

        // Pedido AJAX ao backend
        $.ajax({
            url: '/app/DCW/Controllers/availability.php',
            type: 'POST',
            data: { pickedDate: pickedLeaveDate, iid: iid },
            dataType: 'json'
        })
        .done(function(data) {
            
            if(data.dates.length === 0) {

                canProcessReservationLeave = true
                messageWrapperLeave.html('<p class="success">Data Disponível!</p>')

                // Mostrar inputs de reserva adicionais
                $.ajax({
                    url: '/app/DCW/Controllers/property.php',
                    type: 'POST',
                    data: { iid },
                    dataType: 'json'
                })
                .done(function(data) {

                    spinnerLeave.hide()

                    property = data.property[0]

                    let momentPickedEntryDate = moment(pickedEntryDate)
                    let momentPickedLeaveDate = moment(pickedLeaveDate)
                    
                    let duration = momentPickedLeaveDate.diff(moment(momentPickedEntryDate), 'days')

                    // Atualizar os dias de reserva
                    $('#days').text(`${duration}`)
                    
                    finalPrice = Number(duration * property.preço_diário).toFixed(2)

                    $('#make-reservation-message-wrapper').html(
                        `<p id="finalPrice"><strong>Valor Total:</strong> €${finalPrice}</p>
                            <p id="entryDate"><strong>Data de Entrada:</strong> ${pickedEntryDate}</p>
                            <p id="leaveDate"><strong>Data de Saída:</strong> ${pickedLeaveDate}</p>
                        `
                    )

                    makeReservationWrapper.show()
                
                })
            
            } else {
            
                canProcessReservationLeave = false
                messageWrapperLeave.html('<p class="error">Data Indisponível!</p>')

            }

        })
    }

    function leaveConfirmation() {
        
        $('#info-message-wrapper').html('')
        
        if(canProcessReservationLeave) {

            spinnerLeave.show()

            let numberOfGuests = Number($("#number-of-guests").val())

            // PEDIDO AJAX
            $.ajax({
                url: '/app/DCW/Controllers/reservate.php',
                type: 'POST',
                data: { entryDate: pickedEntryDate, leaveDate: pickedLeaveDate, iid: iid, numberOfGuests: numberOfGuests, price: finalPrice },
                dataType: 'json'
            })
            .done(function(data) {

                spinnerLeave.hide()
                
                if(data.status === 200) {

                    if(numberOfGuests > 0) {
                        window.location = '/guests.php?rid=' + data.rid + '&totalGuests=' + numberOfGuests
                    } else {
                        window.location = '/profile.php?hid=' + hid
                    }
                
                } else {
                    $('#info-message-wrapper').html(`<p class="info"><i class="fas fa-info-circle"></i> ${data.error}</p>`)
                }

            })
        
        }
    }

    function leaveOpen() {
        
        let date = []

        let year = Number(pickedEntryDate.split('-')[0])
        let month = Number(pickedEntryDate.split('-')[1])
        let day =  Number(pickedEntryDate.split('-')[2]) + 1

        date[0] = year
        date[1] = month
        date[2] = day

        if(day === 32 && month === 12) {
            date[0] = `${year + 1}`
            date[1] = '01'
            date[2] = '01'
        }

        if(day === 32 && month === 01 || month === 03 || month === 05 || month === 07 || month === 08 || month === 10 || moment().isLeapYear() && month === 02 && day === 30 || day === 29 && !moment().isLeapYear() || day === 31 && month === 04 || month === 06 || month === 09 || month === 11) {
            date[0] = year
            date[1] = `0${month + 1}`
            date[2] = '01'
        }

        let newDate = date.join('-')

        $("#leave-date-datepicker-reservation").datepicker({
            dateFormat: 'yy-mm-dd',
            minDate: newDate,
            monthNames: [ "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ],
            dayNamesShort: [ "Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb" ]
        })

        $("#leave-date-datepicker-reservation").datepicker('setDate', newDate)
        $("#leave-date-datepicker-reservation").datepicker('option', 'minDate', newDate)
        $("#leave-date-datepicker-reservation").datepicker('refresh')

        spinnerLeave.hide()
        formLeave.show()
    }

    function leaveClose() {
        dialogLeave.dialog("close");
    }

    function makeReservation() {
        
        dialogEntry.dialog("open")

        $("#entry-date-datepicker-reservation").datepicker({
            dateFormat: 'yy-mm-dd',
            defaultDate: Date.now(),
            minDate: 0,
            monthNames: [ "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ],
            dayNamesShort: [ "Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb" ]
        })

        spinnerEntry.hide()
        formEntry.show()
    }

    function formSubmit(event) {
        
        event.preventDefault()

        // Verificar se é possível processar a reserva
        if(canProcessReservationEntry) {
            
            spinnerEntry.show()

            // Pedido AJAX ao backend
            $.ajax({
                url: '/app/DCW/Controllers/reservate.php',
                type: 'POST',
                data: { pickedDate: pickedEntryDate, iid: iid },
                dataType: 'json'
            })
            .done(function(data) {

                spinnerEntry.hide()
                
                if(data.status === 200) {
                    
                    messageWrapperEntry.html('<p class="success">' + data.message + '</p>')
                
                } else {
                
                    messageWrapperEntry.html('<p class="error">' + data.error + '</p>')

                }

            })
            
        }
    
    }

    function openCancelationDialog() {
        cancelDialog.dialog('open')
    }

    function cancelReservation(event) {

        $('#blocker').fadeIn('fast', function() {
            $(this).css('display', 'flex')
        })

        $.ajax({
            url: '/app/DCW/Controllers/cancel.php',
            type: 'POST',
            data: { rid },
            dataType: 'json'
        })
        .done(function(data) {
            window.location = '/profile.php?hid=' + data.hid
        })

    }

    let dialogEntry = $( "#entry-date-reservation-dialog-form" ).dialog({
        autoOpen: false,
        draggable: false,
        height: 450,
        width: 350,
        modal: true,
        buttons: {
            Verificar: entryVerification,
            Confirmar: entryConfirmation,
            Fechar: entryClose
        },
        close: function() {

            formEntry.hide()
            spinnerEntry.show()

        }
    })

    let dialogLeave = $( "#leave-date-reservation-dialog-form" ).dialog({
        autoOpen: false,
        draggable: false,
        height: 550,
        width: 350,
        modal: true,
        buttons: {
            Verificar: leaveVerification,
            Confirmar: leaveConfirmation,
            Fechar: leaveClose
        },
        open: leaveOpen,
        close: function() {

            formLeave.hide()
            spinnerLeave.show()

        }
    
    })

    // Verificar no backend as datas disponíveis para reserva
    $("#make-reservation").on("click", makeReservation)

    // Submeter o formulário
    $("#entry-date-reservation-form").on('submit', formSubmit)

    // Cancelar reserva
    $('#cancel-reservation').on('click', openCancelationDialog)

    // Menu - Barra de navegação
    let menu = $('#menu-icon')
    let menuList = $('#menu-list')
    let isOpen = false

    menu.on('click', toggle)

    // Forçar a reposição da barra de navegação normal
    $(window).resize(function() {

        if($(this).innerWidth() > 600) {
            menuList.css('display', 'flex')
        } else {
            menuList.css('display', 'none')
        }

    })

    function toggle() {

        if(isOpen === false) {
            
            menuList.slideDown('fast', function() {
                $(this).css('display', 'flex')
            })
            
            isOpen = true
        
        } else {

            menuList.slideUp('fast', function() {
                $(this).css('display', 'none')
            })
            
            isOpen = false

        }

    }

    // Pesquisa
    let resultsWrapper = $('#search-wrapper .results-wrapper')
    let searchWrapper = $('#search-wrapper')
    let loadingAndState = $('#loading-and-state')
    let isShowing = false
    
    function closeSearch() {
        
        searchWrapper.hide('fast', function() {

            resultsWrapper.html('')
            loadingAndState.html('')
            isShowing = false

        })
    
    }

    function openSearch() {

        searchWrapper.show('fast', function() {

            resultsWrapper.html('')
            loadingAndState.html('')
            isShowing = true

            if(isOpen && window.innerWidth <= 600) {
                toggle()
            }

        })

    }

    $('#open-search').on('click', openSearch)

    $('#close-search').on('click', closeSearch)

    $('#search').on('keyup', function() {

        let search = $(this).val()

        // Apenas procurar se a pesquisa tiver mais de 3 caractéres - evita o spam search
        if(search.length > 3) {

            if(!isShowing) {

                searchWrapper.show('fast', function() {
                    isShowing = true   
                })
            }

            loadingAndState.html(
                `
                <div class="spinner" id="search-spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
                `
            )

            $.ajax({
                url: '/app/DCW/Controllers/search.php',
                type: 'POST',
                data: { search },
                dataType: 'json'
            })
            .done(function(data) {

                resultsWrapper.html('')

                if(data.status === 200) {

                    if(data.results.length === 0) {
                        loadingAndState.html('<p><i class="fas fa-info-circle"></i> Sem resultados!</p>')
                    } else {

                        loadingAndState.html('')

                        data.results.forEach(result => {

                            resultsWrapper.append(
                                `
                                <a href="/property.php?iid=${ result.iid }">
                                    <div class="result">
                                        <div class="photo-wrapper">
                                            <img src="${ result.foto_principal }" alt="main-photo">
                                        </div>
                                        <div class="title-wrapper">
                                            <h3>${ result.título }</h3>
                                        </div>
                                        <div class="address-and-city-wrapper">
                                            <p>${ result.localização } - ${ result.cidade }</p>
                                        </div>
                                        <div class="characteristics-wrapper">
                                            <p><i class="fas fa-building"></i> ${ result.tipologia }</p>
                                            <p><i class="fas fa-users"></i> ${ result.lotação }</p>
                                            ${ result.animais == 1 ? '<p><i class="fas fa-paw"></i></p>' : '<p><i style="color: silver;" class="fas fa-paw"></i></p>' }
                                            ${ result.crianças == 1 ? '<p><i class="fas fa-baby"></i></p>' : '<p><i style="color: silver;" class="fas fa-baby"></i></p>' }
                                        </div>
                                        <div class="price-wrapper">
                                            <p>€ ${ result.preço_diário } / dia</p>
                                        </div>
                                    </div>
                                </a>
                                `
                            )

                        })

                    }

                }

            })

        }

    })

    // Carregar mais ofertas
    let index = 1

    $('#load-more').click(function() {

        $('#loading-more-spinner').fadeIn('fast')

        let offset = (index * 10) - 1

        $.ajax({
            url: '/app/DCW/Controllers/offers.php',
            type: 'POST',
            data: { offset },
            dataType: 'json'
        })
        .done(function(data) {

            $('#loading-more-spinner').fadeOut('slow')

            if(data.length > 0) {

                data.forEach(property => {

                    // Atualizar a lista para o fuzzy search
                    list.push(property)

                    $('.offers-wrapper').append(
                        `
                            <div class="offer">
                                <div class="thumbnail-wrapper">
                                    <img src=${ property.foto_principal } alt="imagem da oferta">
                                </div>
                                <div class="title">
                                    <h2>${ property.título }</h2>
                                </div>
                                <div class="caracteristics">
                                    <p><i class="fas fa-building"></i> - ${ property.tipologia }</p>
                                    <p><i class="fas fa-users"></i> - ${ property.lotação }</p>
                                </div>
                                <div class="address">
                                    <p>${ property.localização } - ${ property.cidade }</p>
                                </div>
                                <div class="description">
                                    <p>${ limitText(property.descrição, 200) }</p>
                                </div>
                                <div class="price">
                                    <small>€ ${ property.preço_diário } / dia</small>
                                </div>
                                <div class="created-at">
                                    <small><i class="fas fa-info-circle"></i> Adicionado a ${ property.adicionado_a } </small>
                                </div>
                                <div class="offer-actions">
                                    <a class="know-more-btn" href="/property.php?iid=${ property.iid }">Reservar! <i class="fas fa-chevron-right"></i></a>
                                </div>
                            </div>
                        `
                    )
                
                })

                index++

            } else {

                $('.load-more-wrapper').html('<p>De momento, não existem mais ofertas!</p>')

            }

        })

    })

    function limitText(text, limit) {
        return text.substring(text, limit) + '...'
    }

    function populate(filter) {

        let value = filter.target.value

        if(value.length > 0) {
            
            let results = fuse.search(value)
            
            $('.offers-wrapper').html('')

            results.forEach(property => {

                $('.offers-wrapper').append(
                    `
                        <div class="offer">
                            <div class="thumbnail-wrapper">
                                <img src=${ property.foto_principal } alt="imagem da oferta">
                            </div>
                            <div class="title">
                                <h2>${ property.título }</h2>
                            </div>
                            <div class="caracteristics">
                                <p><i class="fas fa-building"></i> - ${ property.tipologia }</p>
                                <p><i class="fas fa-users"></i> - ${ property.lotação }</p>
                            </div>
                            <div class="address">
                                <p>${ property.localização } - ${ property.cidade }</p>
                            </div>
                            <div class="description">
                                <p>${ limitText(property.descrição, 200) }</p>
                            </div>
                            <div class="price">
                                <small>€ ${ property.preço_diário } / dia</small>
                            </div>
                            <div class="created-at">
                                <small><i class="fas fa-info-circle"></i> Adicionado a ${ property.adicionado_a } </small>
                            </div>
                            <div class="offer-actions">
                                <a class="know-more-btn" href="/property.php?iid=${ property.iid }">Reservar! <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    `
                )

            })

        } else {

            $('.offers-wrapper').html('')

            list.forEach(property => {

                $('.offers-wrapper').append(
                    `
                        <div class="offer">
                            <div class="thumbnail-wrapper">
                                <img src=${ property.foto_principal } alt="imagem da oferta">
                            </div>
                            <div class="title">
                                <h2>${ property.título }</h2>
                            </div>
                            <div class="caracteristics">
                                <p><i class="fas fa-building"></i> - ${ property.tipologia }</p>
                                <p><i class="fas fa-users"></i> - ${ property.lotação }</p>
                            </div>
                            <div class="address">
                                <p>${ property.localização } - ${ property.cidade }</p>
                            </div>
                            <div class="description">
                                <p>${ limitText(property.descrição, 100) }</p>
                            </div>
                            <div class="price">
                                <small>€ ${ property.preço_diário } / dia</small>
                            </div>
                            <div class="created-at">
                                <small><i class="fas fa-info-circle"></i> Adicionado a ${ property.adicionado_a } </small>
                            </div>
                            <div class="offer-actions">
                                <a class="know-more-btn" href="/property.php?iid=${ property.iid }">Reservar! <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    `
                )
            
            })

        }
    }

    // Filtro - FuseJS - http://fusejs.io
    let options = {
        shouldSort: true,
        threshold: 0.6,
        location: 0,
        distance: 100,
        maxPatternLength: 32,
        minMatchCharLength: 5,
        keys: [
            { name: 'título', weight: 0.6 },
            { name: 'cidade', weight: 0.6 },
            { name: 'localização', weight: 0.4 },
            { name: 'animais', weight: 0.2 },
            { name: 'crianças', weight: 0.2 }
        ]
    }

    let list = []

    // Buscar os primeiros 10 imóveis
    $.ajax({
        url: '/app/DCW/Controllers/offers.php',
        type: 'POST',
        data: { offset: 0 },
        dataType: 'json'
    })
    .done(function(data) {

        if(data.length > 0) {
            data.map(property => list.push(property))
        }

    })

    let fuse = new Fuse(list, options)
    let filter = $('#filter')

    filter.on('keyup', populate)

    // Page Tracker
    let page = window.location.pathname.split('/')[1].split('.')[0].length > 0 ? window.location.pathname.split('/')[1].split('.')[0] : 'home'

    if (typeof(Storage) !== "undefined") {
    
        window.localStorage.setItem('page', page)

    }

    $('nav ul li a').each(function() {

        let page = $(this)

        if(page.attr('data-page') !== undefined) {

            if(page.attr('data-page') === window.localStorage.getItem('page')) {
                page.addClass('active')
            } else {
                page.removeClass('active')
            }

        }

    })
    

})