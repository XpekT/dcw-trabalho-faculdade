<?php 

require 'vendor/autoload.php';

use DCW\Classes\Session;
use DCW\Models\Property;

Session::start();

$property = new Property();

$chunk = $property->chunk(0);

?>

<?php include_once(__DIR__ . '/includes/imports.php') ?>

		<?php include_once(__DIR__ . '/includes/nav.php') ?>
		
		<div class="main-container">

            <div class="header">
				<h1 style="border: none;">Venha conhecer as nossa ofertas!</h1>
			</div>
			<div class="filter-wrapper">
				<input type="text" id="filter" placeholder="Filtrar...">
			</div>
			<div class="offers-main">
				<?php if(count($chunk) > 0): ?>
					<div class="wrapper">	
						<div class="offers-wrapper">
							<?php foreach($chunk as $property): ?>
								<div class="offer">
									<div class="thumbnail-wrapper">
										<img src=<?php echo $property['foto_principal']; ?> alt="imagem da oferta">
									</div>
									<div class="title">
										<h2><?php echo $property['título']; ?></h2>
									</div>
									<div class="caracteristics">
										<p><i class="fas fa-building"></i> - <?php echo $property['tipologia']; ?></p>
										<p><i class="fas fa-users"></i> - <?php echo $property['lotação']; ?></p>
									</div>
									<div class="address">
										<p><?php echo $property['localização']; ?> - <?php echo $property['cidade']; ?></p>
									</div>
									<div class="description">
										<p><?php echo mb_substr($property['descrição'], 0, 200) . '...'; ?></p>
									</div>
									<div class="price">
										<small>€ <?php echo $property['preço_diário']; ?> / dia</small>
									</div>
									<div class="created-at">
										<small><i class="fas fa-info-circle"></i> Adicionado a <?php echo $property['adicionado_a']; ?></small>
									</div>
									<div class="offer-actions">
										<a class="know-more-btn" href=<?php echo "/property.php?iid=" . $property['iid'] ?>>Reservar! <i class="fas fa-chevron-right"></i></a>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				<?php endif; ?>
				<div class="load-more-wrapper">
					<div class="spinner" id="loading-more-spinner">
						<div class="bounce1"></div>
						<div class="bounce2"></div>
						<div class="bounce3"></div>
					</div>
					<i class="fas fa-plus-circle" id="load-more"></i>
				</div>
			</div>
        </div>

		<?php include_once(__DIR__ . '/includes/footer.php') ?>

		</body>

</html>