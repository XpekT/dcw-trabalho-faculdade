<?php 

require "vendor/autoload.php";

use DCW\Classes\Session;
use DCW\Models\User;
use DCW\Helpers\Utils;

Session::start();

$countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
// Verificar se o parâmetro hid está definido.
// Se não estiver redirecionar para a página accounts.php
$hid = $_GET['hid'];

if(!isset($hid) || empty($hid) || mb_strlen($hid) === 0) {
    
    header('Location: /accounts.php');

    exit();

} else {
    
    // Verificar se o utilizador tem sessão iniciada
    $userSession = Session::get('session-user');
    
    if(isset($userSession)) {
    
        // Verificar se utilizador com sessão iniciada não detêm o perfil procurado
        if((int)$hid !== (int)$userSession['hid']) {
            
            header("Location: /accounts.php");
    
            exit();
        } else {

            // Fazer uma query à BD para as reservas do utilizador
            $user = new User();
            $reservations = $user->reservations($userSession['hid']);

        }
    
    } else {
    
        header("Location: /accounts.php");
        
        exit();
    }
}

?>

<?php include_once(__DIR__ . '/includes/imports.php') ?>

        <?php include_once(__DIR__ . '/includes/nav.php') ?>

        <div class="main-container">

            <div class="user-data-wrapper">

                <div class="user-details">
                    <h1>Dados Pessoais</h1>
                    <p><strong>Nome</strong>: <?php echo $userSession['nome']; ?></p>
                    <p><strong>Data de Nascimento: </strong><?php echo $userSession['data_nascimento']; ?></p>
                    <p><strong>Email: </strong><?php echo $userSession['email']; ?></p>
                    <p><strong>Nacionalidade: </strong><?php echo $userSession['nacionalidade']; ?></p>
                    <p><strong>País: </strong><?php echo $userSession['país']; ?></p>
                    <p><strong>Cidade: </strong><?php echo $userSession['cidade']; ?></p>
                    <p><strong>Cartão de Cidadão: </strong><?php echo !empty($userSession['cartão_cidadão']) ? $userSession['cartão_cidadão'] : 'N/D'; ?></p>
                    <p><strong>Passaporte: </strong><?php echo !empty($userSession['passaporte']) ? $userSession['passaporte'] : 'N/D'; ?></p>
                    <div class="button-wrapper">
                        <button class="edit-btn" id="edit-account-btn" data-hid=<?php echo $hid; ?>><i class="fas fa-cog"></i> Editar!</button>
                        <button class="delete-account-btn" id="delete-account-btn" data-hid=<?php echo $hid; ?>><i class="fas fa-skull-crossbones"></i> Apagar Conta!</button>
                    </div>
                </div>
    
                <div class="user-services">
                    <h1>Reservas Efetuadas</h1>
                    <?php if(count($reservations) > 0): ?>
                        <table>
                            <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Preço Total</th>
                                    <th>Data de Entrada</th>
                                    <th>Data de Saída</th>
                                    <th>Titular</th>
                                    <th>Hóspedes Adicionais</th>
                                    <th>Efetuada a</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($reservations as $reservation): ?>
                                    <tr>
                                        <td><a href=<?php echo "/reservation.php?rid=". $reservation['rid'] ."" ?>><?php echo $reservation['código']; ?></a></td>
                                        <td>€<?php echo $reservation['valor_total']; ?></td>
                                        <td><?php echo Utils::dateFormat($reservation['data_entrada']); ?></td>
                                        <td><?php echo Utils::dateFormat($reservation['data_saída']); ?></td>
                                        <td><?php echo $reservation['nome'] === $userSession['nome'] ? $userSession['nome'] : $reservation['nome']; ?></td>
                                        <td><?php echo $reservation['hóspedes_adicionais']; ?></td>
                                        <td><?php echo $reservation['adicionado_a']; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php else: ?>
                        <p><i class="fas fa-info-circle"></i> Não existem reservas efetuadas!</p>
                    <?php endif; ?>
                </div>
            
            </div>

            <!-- Confirmação da eliminação de conta - MODAL -->
            <div id="delete-account-dialog-confirm" title="Apagar Conta?">
                <i class="fas fa-exclamation-triangle"></i>
                <p>A eliminação é irreversível!</p>
            </div>

            <!-- Edição de conta - MODAL -->
            <div id="edit-account-dialog-form" title="Editar Dados Pessoais">
                <form id="edit-account-form">
                    <div class="input-wrapper">
                        <label for="name">Nome</label>
                        <input type="text" name="nome" id="name" value="<?php echo $userSession['nome']; ?>" class="text ui-widget-content ui-corner-all">
                    </div>
                    <div class="input-wrapper">
                        <label for="dob">Data de Nascimento</label>
                        <input id="datepicker-edit-form" type="text" name="data_nascimento" id="dob" value="<?php echo $userSession['data_nascimento']; ?>" class="text ui-widget-content ui-corner-all">
                    </div>
                    <div class="input-wrapper">
                        <label for="email">Email</label>
                        <input type="text" name="email" id="email" value="<?php echo $userSession['email']; ?>" class="text ui-widget-content ui-corner-all">
                    </div>
                    <div class="input-wrapper">
                        <label for="nationality">Nacionalidade</label>
                        <select type="nationality" name="nacionalidade" id="nationality">
                            <?php  foreach($countries as $country): ?>
                                <?php if($country === $userSession['país']): ?>
                                    <?php echo '<option value='.$country.' selected>'.$country.'</option>'; ?>
                                <?php else: ?>
                                    <?php echo '<option value='.$country.'>'.$country.'</option>'; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="input-wrapper">
                        <label for="country">País</label>
                        <select type="country" name="país" id="country">
                            <?php  foreach($countries as $country): ?>
                                <?php if($country === $userSession['nacionalidade']): ?>
                                    <?php echo '<option value='.$country.' selected>'.$country.'</option>'; ?>
                                <?php else: ?>
                                    <?php echo '<option value='.$country.'>'.$country.'</option>'; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="input-wrapper">
                        <label for="city">Cidade</label>
                        <input type="text" name="cidade" id="city" value="<?php echo $userSession['cidade']; ?>" class="text ui-widget-content ui-corner-all">
                    </div>
                    <div class="input-wrapper">
                        <label for="cc">Cartão de Cidadão</label>
                        <input type="text" name="cartão_cidadão" id="cc" value="<?php echo $userSession['cartão_cidadão']; ?>" class="text ui-widget-content ui-corner-all">
                    </div>
                    <div class="input-wrapper">
                        <label for="passport">Passaporte</label>
                        <input type="text" name="passporte" id="passport" value="<?php echo $userSession['passaporte']; ?>" class="text ui-widget-content ui-corner-all">
                    </div>
                    <div class="input-wrapper">
                        <label for="password">Password</label>
                        <input type="password" name="password" id="password" value='' class="text ui-widget-content ui-corner-all">
                    </div>
                    <div class="input-wrapper">
                        <label for="password-confirmation">Confirmação de Password</label>
                        <input type="password" name="password-confirmation" id="password-confirmation" value='' class="text ui-widget-content ui-corner-all">
                    </div>
                    <div class="edit-form-message-wrapper"></div>
                    <!-- Allow form submission with keyboard without duplicating the dialog button -->
                    <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
                </form>
            </div>

        </div>

        <?php include_once(__DIR__ . '/includes/footer.php') ?>

        <script type="text/javascript">

            $(function() {
                
                // Modal de confirmação de eliminação de conta
                let deleteDialog = $("#delete-account-dialog-confirm").dialog({
                    resizable: false,
                    autoOpen: false,
                    draggable: false,
                    height: "auto",
                    width: 400,
                    modal: true,
                    buttons: {
                        Confirmar: deleteAccount,
                        Cancelar: function() {
                            $(this).dialog("close");
                        }
                    }
                })

                // Modal de confirmação de edição de conta
                let editDialog = $("#edit-account-dialog-form").dialog({
                    resizable: false,
                    autoOpen: false,
                    draggable: false,
                    height: "auto",
                    width: 400,
                    modal: true,
                    buttons: {
                        Confirmar: editAccount,
                        Cancelar: function() {
                            $(this).dialog("close");
                        }
                    }
                })

                function deleteAccount() {

                    let hid = $('#delete-account-btn').attr('data-hid')

                    $('#blocker').fadeIn('fast')

                    $.ajax({
                        url: '/app/DCW/Controllers/delete.php',
                        type: 'POST',
                        data: { hid },
                        dataType: 'json'
                    })
                    .done(function(data) {
                        window.location = '/'
                    })

                }

                function editAccount(event) {

                    event.preventDefault()

                    let data = $('#edit-account-form').serializeArray()

                    let hid = $('#delete-account-btn').attr('data-hid')

                    let messageWrapper = $('.edit-form-message-wrapper')

                    messageWrapper.html('')

                    $.ajax({
                        url: '/app/DCW/Controllers/edit.php',
                        type: 'POST',
                        data: { hid, data },
                        dataType: 'json',
                        beforeSend: function() {

                            let isValid = false

                            if(data[8].value.trim().length !== 0 && data[8].value !== data[9].value) {
                               
                                messageWrapper.html('<p class="error"><i class="fas fa-info-circle"></i> As passwords não são iguais!</p>')
                            
                            } else if(data[6].value.trim().length === 0 && data[7].value.trim().length === 0) {

                                messageWrapper.html('<p class="error"><i class="fas fa-info-circle"></i> Tem de fornecer um método de identificação!</p>')

                            } 
                            else {

                                $('#blocker').fadeIn('fast')
                                
                                isValid = true
                           
                            }

                            return isValid

                        }
                    })
                    .done(function(data) {

                        if(data.redirect) {
                            window.location = '/'
                        }

                        $('#blocker').fadeOut('slow')

                        if(data.status === 200) {
                            location.reload()
                        } else {
                            messageWrapper.html('<p class="error"><i class="fas fa-info-circle"></i> ' + data.error + '</p>')
                        }

                    })

                }

                // Abrir o Modal - Eliminação de conta
                $('#delete-account-btn').on('click', function() {
                    deleteDialog.dialog('open')
                })

                // Abrir o Modal - Edição de conta
                $('#edit-account-btn').on('click', function() {
                    editDialog.dialog('open')
                })

                // Datepicker do formulário de edição
                $('#datepicker-edit-form').datepicker({
                    maxDate: 0
                })

                $('#edit-account-form').on('submit', function(event) {
                    event.preventDefault()
                })

            })


        </script>

    </body>

</html>