<?php 

require "vendor/autoload.php";

use DCW\Models\Property;
use DCW\Classes\Session;

Session::start();

if(isset($_GET['iid'])) {

    $userSession = Session::get('session-user');

    // Buscar imóvel
    $property = new Property();

    $data = isset($property->get($_GET['iid'])[0]) ? $property->get($_GET['iid']) : [];
    $unavailableDates = $property->getReservedDates($_GET['iid']);


} else {

    header('Location: /');

    exit();

}

?>

<?php include_once(__DIR__ . '/includes/imports.php') ?>

        <?php include_once(__DIR__ . '/includes/nav.php') ?>

        <div class="main-container" id="main-container" data-iid=<?php echo $_GET['iid']; ?> data-hid=<?php echo $userSession['hid']; ?>>

            <div class="property-wrapper">
                <?php if(count($data) > 0): ?>
                    <div class="property-main-header">
                        <div class="title">
                            <h1><?php echo $data[0]['título']; ?></h1>
                        </div>
                        <div class="main-photo">
                            <img src=<?php echo $data[0]['foto_principal']; ?> alt="main-photo-property">
                        </div>
                    </div>
                    <div class="property-description">
                        <p><i class="fas fa-quote-left"></i> <?php echo $data[0]['descrição']; ?> <i class="fas fa-quote-right"></i></p>
                    </div>
                    <div class="property-characteristics">
                        <h1>Características Principais</h1>
                        <div class="characteristics-wrapper">
                            <p><strong>Tipologia: </strong><?php echo $data[0]['tipologia']; ?></p>
                            <p><strong>Lotação: </strong><?php echo $data[0]['lotação']; ?></p>
                            <p><strong>Crianças: </strong><?php echo (int)$data[0]['crianças'] === 0 ? 'Não' : 'Sim'; ?></p>
                            <p><strong>Animais: </strong><?php echo (int)$data[0]['animais'] === 0 ? 'Não' : 'Sim'; ?></p>
                        </div>
                    </div>
                    <div class="property-images">
                        <h1>Veja algumas fotos do imóvel!</h1>
                        <div class="secundary-photos-wrapper">
                            <div class="property-photo">
                                <img class="secondary-photo" src=<?php echo $data[0]['foto_secundária_1']; ?> alt="secundary-photo">
                            </div>
                            <div class="property-photo">
                                <img class="secondary-photo" src=<?php echo $data[0]['foto_secundária_2']; ?> alt="secundary-photo">
                            </div>
                            <div class="property-photo">
                                <img class="secondary-photo" src=<?php echo $data[0]['foto_secundária_3']; ?> alt="secundary-photo">
                            </div>
                            <div class="property-photo">
                                <img class="secondary-photo" src=<?php echo $data[0]['foto_secundária_4']; ?> alt="secundary-photo">
                            </div>
                        </div>
                    </div>
                    <div class="property-city-and-address">
                        <h1>Localização</h1>
                        <div class="address-and-city-wrapper">
                            <div class="city-wrapper">
                                <h3><?php echo $data[0]['cidade']; ?> - <?php echo $data[0]['localização']; ?></h3>
                            </div>
                            <div class="address-wrapper">
                            <?php echo '<iframe scrolling="no" title="Google Maps" aria-label="Google Maps" src="https://static.parastorage.com/services/santa/1.5523.4/static/external/googleMap.html?language=pt-BR&amp;address=' . $data[0]["localização"] . '=&amp;addressInfo=' . $data[0]["título"] . '&amp;showZoom=true&amp;showStreetView=true&amp;showMapType=true" width="100%" height="100%" frameborder="0"></iframe>'; ?>
                            </div>
                        </div>
                    </div>
                    <div class="unavailable-dates">
                        <h1>Datas Indisponíveis</h1>
                        <div class="unavailable-dates-wrapper">
                            <?php if(count($unavailableDates) > 0): ?>
                                <table>
                                    <thead>
                                        <th>Data de Entrada</th>
                                        <th></th>
                                        <th>Data de Saída</th>
                                    </thead>
                                    <tbody>
                                    <?php foreach($unavailableDates as $date): ?>
                                        <tr>
                                            <td><?php echo $date['data_entrada']; ?></td>
                                            <td><i class="fas fa-arrow-right"></i></td>
                                            <td><?php echo $date['data_saída']; ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            <?php else: ?>
                                <p><i class="fas fa-info-circle"></i> Não existem datas indisponíveis!</p>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php if(isset($userSession)): ?>
                        <div class="daily-price-and-reservation-button-wrapper">
                            <p>€ <?php echo $data[0]['preço_diário']; ?> / dia</p>
                            <button type="button" id="make-reservation"><i class="fas fa-check-circle"></i> Reservar!</button>
                        </div>
                    <?php endif; ?>
                <?php else: ?>
                    <div class="property-info-wrapper">
                        <h1><i class="fas fa-info-circle"></i> Este imóvel não existe ou foi removido!</h1>
                    </div>
                <?php endif; ?>
            </div>

            <!-- Ações - Modal Data de Entrada -->
            <div id="entry-date-reservation-dialog-form" title="Fazer Reserva">
                <div class="spinner" id="entry-date-reservation-spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
                <form id="entry-date-reservation-form">
                    <fieldset>
                        <span id="entry-date-datepicker-reservation"><span/>
                        <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
                        <div id="entry-date-reservation-form-message-wrapper">
                            <p>Escolha a data de entrada!</p>
                        </div>
                    </fieldset>
                </form>
            </div>

            <!-- Ações - Modal Data de Saída -->
            <div id="leave-date-reservation-dialog-form" title="Fazer Reserva">
                <div class="spinner" id="leave-date-reservation-spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
                <form id="leave-date-reservation-form">
                    <fieldset>
                        <span id="leave-date-datepicker-reservation"><span/>
                        <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
                        <div id="leave-date-reservation-form-message-wrapper">
                            <p>Escolha a data de saída!</p>
                        </div>
                    </fieldset>
                </form>

                <div id="make-reservation-inputs-wrapper">
                    <fieldset>
                        <legend>Dados da Reserva</legend>
                        <div class="controlgroup">
                            <div id="reservation-days-wrapper">
                                <label for="days"><strong>Dias: </strong></label>
                                <span id="days"></span>
                            </div>
                            <div id="make-reservation-message-wrapper"></div>
                            <div id="guests-wrapper">
                                <label for="number-of-guests" style="font-weight: bold;"># de Hóspedes Adicionais</label>
                                <select id="number-of-guests" >
                                    <option value="0">+0</option>
                                    <option value="1">+1</option>
                                    <option value="2">+2</option>
                                    <option value="3">+3</option>
                                    <option value="4">+4</option>
                                    <option value="5">+5</option>
                                    <option value="6">+6</option>
                                    <option value="7">+7</option>
                                    <option value="8">+8</option>
                                    <option value="9">+9</option>
                                    <option value="10">+10</option>
                                </select>
                                <div id="info-message-wrapper"></div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>

            <!-- Modal - Imagens -->
            <div id="dialog-modal-images" title="Imóvel">
                <div class="image-wrapper"></div>
            </div>

        </div>

    <?php include_once(__DIR__ . '/includes/footer.php') ?>

    <script type="text/javascript">
    
        let width = window.innerWidth - 100
        let maxHeight = window.innerHeight - 100

        let dialogImages = $('#dialog-modal-images').dialog({
            autoOpen: false,
            modal: true,
            draggable: false,
            resizable: true,
            width,
            maxHeight,
            show: {
                effect: 'fade',
                duration: 300
            },
            open: function() {
                $('.modal-image').css('height', maxHeight)
            }
        })

        $(window).on('resize', function(event) {

            width = event.target.innerWidth - 100
            maxHeight = event.target.innerheight - 100

            dialogImages.dialog("option", "width", width)
            dialogImages.dialog("option", "maxHeight", maxHeight)

            $('.modal-image').css('height', maxHeight)

        })

        $('.secondary-photo').on('click', function(event) {

            let src = event.target.src

            $('#dialog-modal-images').find('.image-wrapper').html(`
                <img class="modal-image" src=${src} alt="property-photo" />
            `)

            dialogImages.dialog("option", "height", "auto")

            dialogImages.dialog('open')

        })

    </script>

    </body>

</html>