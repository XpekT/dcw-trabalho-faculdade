<?php 

require "vendor/autoload.php";

use DCW\Classes\Session;
use DCW\Models\Reservation;
use DCW\Helpers\Utils;

Session::start();

// Verificar se o parâmetro rid está definido.
// Se não estiver redirecionar para a página index.php
$rid = $_GET['rid'];

if(!isset($rid) || empty($rid) || mb_strlen($rid) === 0) {
    
    header("Location: /");

    exit();

} else {

    // Verificar se o utilizador tem sessão iniciada
    $userSession = Session::get('session-user');

    if(isset($userSession)) {

        $fullReservationDetails = [];

        // Verificar se o utilizador pertence ou detêm esta reserva
        $reservation = new Reservation();

        $result = $reservation->belongsToReservation($rid, $userSession['hid']);

        if($result) {

            // O utilizador pertence ou detém esta reserva - buscar a reserva
            $fullReservationDetails = $reservation->getReservation($rid);

        } else {

            header("Location: /");

            exit();

        }
 
    } else {

        header("Location: /");

        exit();

    }

}
?>

<?php include_once(__DIR__ . '/includes/imports.php') ?>

        <?php include_once(__DIR__ . '/includes/nav.php') ?>

        <div class="main-container" id="main-container" data-iid=<?php echo $fullReservationDetails[0]['iid']; ?> data-rid=<?php echo $fullReservationDetails[0]['rid']; ?> data-hid=<?php echo $userSession['hid']; ?>>

            <?php if(count($fullReservationDetails) !== 0): ?>
                <div class="reservation-table-wrapper">
                    <h1>Reserva - <?php echo $fullReservationDetails[0]['código']; ?></h1>
                    <table id="reservation-table">
                        <thead>
                            <th>Imóvel</th>
                            <th>Localização</th>
                            <th>Entrada</th>
                            <th>Saída</th>
                            <th>Hóspedes</th>
                            <th>Titular</th>
                            <th>Total</th>
                            <th>Estado</th>
                            <th>Ação</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="width: 300px;">
                                    <a href=<?php echo '/property.php?iid=' . $fullReservationDetails[0]['iid']; ?>>
                                        <img src=<?php echo $fullReservationDetails[0]['foto_principal']; ?> alt="imóvel-foto" />
                                    </a>
                                </td>
                                <td><?php echo $fullReservationDetails[0]['localização']; ?></td>
                                <td><?php echo Utils::dateFormat($fullReservationDetails[0]['data_entrada']); ?></td>
                                <td><?php echo Utils::dateFormat($fullReservationDetails[0]['data_saída']); ?></td>
                                <td>
                                    <?php foreach($fullReservationDetails as $detail): ?>
                                        <p><?php echo $detail['nome'] === $userSession['nome'] ? 'Você' : $detail['nome']; ?></p>
                                    <?php endforeach; ?>
                                </td>
                                <td>
                                    <?php foreach($fullReservationDetails as $detail): ?>
                                        <?php if($detail['hid'] === $detail['titular']): ?>
                                            <p><?php echo $detail['nome'] === $userSession['nome'] ? 'Você' : $detail['nome']; ?></p>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </td>
                                <td>€<?php echo $fullReservationDetails[0]['valor_total']; ?></td>
                                <td>
                                    <?php if(Utils::isLaterThan($fullReservationDetails[0]['data_saída'])): ?>
                                        Confirmada!
                                    <?php else: ?>
                                        Concluída!
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php if(Utils::isLaterThan($fullReservationDetails[0]['data_saída']) && $fullReservationDetails[0]['titular'] === $userSession['hid']): ?>
                                        <i id="cancel-reservation" class="fas fa-trash-alt"></i> 
                                    <?php endif; ?>
                                    <?php if(Utils::isLaterThan($fullReservationDetails[0]['data_saída']) && $fullReservationDetails[0]['titular'] !== $userSession['hid'] || !Utils::isLaterThan($fullReservationDetails[0]['data_saída'])): ?>
                                        <i id="make-reservation" class="fas fa-redo"></i>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <!-- Ações - Modal Data de Entrada -->
                <div id="entry-date-reservation-dialog-form" title="Fazer Reserva">
                    <div class="spinner" id="entry-date-reservation-spinner">
						<div class="bounce1"></div>
						<div class="bounce2"></div>
						<div class="bounce3"></div>
					</div>
                    <form id="entry-date-reservation-form">
                        <fieldset>
                            <span id="entry-date-datepicker-reservation"><span/>
                            <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
                            <div id="entry-date-reservation-form-message-wrapper">
                                <p>Escolha a data de entrada!</p>
                            </div>
                        </fieldset>
                    </form>
                </div>

                <!-- Ações - Modal Data de Saída -->
                <div id="leave-date-reservation-dialog-form" title="Fazer Reserva">
                    <div class="spinner" id="leave-date-reservation-spinner">
						<div class="bounce1"></div>
						<div class="bounce2"></div>
						<div class="bounce3"></div>
					</div>
                    <form id="leave-date-reservation-form">
                        <fieldset>
                            <span id="leave-date-datepicker-reservation"><span/>
                            <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
                            <div id="leave-date-reservation-form-message-wrapper">
                                <p>Escolha a data de saída!</p>
                            </div>
                        </fieldset>
                    </form>

                    <div id="make-reservation-inputs-wrapper">
                        <fieldset>
                            <legend>Dados da Reserva</legend>
                            <div class="controlgroup">
                                <div id="reservation-days-wrapper">
                                    <label for="days"><strong>Dias: </strong></label>
                                    <span id="days"></span>
                                </div>
                                <div id="make-reservation-message-wrapper"></div>
                                <div id="guests-wrapper">
                                    <label for="number-of-guests" style="font-weight: bold;"># de Hóspedes Adicionais</label>
                                    <select id="number-of-guests" >
                                        <option value="0">+0</option>
                                        <option value="1">+1</option>
                                        <option value="2">+2</option>
                                        <option value="3">+3</option>
                                        <option value="4">+4</option>
                                        <option value="5">+5</option>
                                        <option value="6">+6</option>
                                        <option value="7">+7</option>
                                        <option value="8">+8</option>
                                        <option value="9">+9</option>
                                        <option value="10">+10</option>
                                    </select>
                                    <div id="info-message-wrapper"></div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>

                <!-- Cancelar Reservar - Modal -->
                <div id="cancel-reservation-dialog-confirm" title="Cancelar Reserva?">
                    <i class="fas fa-exclamation-triangle"></i>
                    <p> O cancelamento da reserva é irreversível!</p>
                </div>

            <?php endif; ?>
        </div>

        <?php include_once(__DIR__ . '/includes/footer.php') ?>

    </body>

</html>